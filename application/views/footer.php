       <footer id="footer">
            <div class="footer_bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="footer_bootomLeft">
                                <p> Copyright © 2023 HR PT. Jakarta Prima Crane</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="footer_bootomRight">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>


        <script async="" src="<?=base_url('themes/assets');?>/analytics.js"></script><script src="<?=base_url('themes/assets');?>/jquery.min.js" type="text/javascript"></script>
        <script src="<?=base_url('themes/assets');?>/wow.min.js" type="text/javascript"></script>
        <script src="<?=base_url('themes/assets');?>/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?=base_url('themes/assets');?>/slick.min.js" type="text/javascript"></script>
        <script src="<?=base_url('themes/assets');?>/jquery.easing.1.3.js" type="text/javascript"></script>
        <script src="<?=base_url('themes/assets');?>/jquery.animate-enhanced.min.js" type="text/javascript"></script>
        <script src="<?=base_url('themes/assets');?>/jquery.superslides.min.js" type="text/javascript"></script>
        <script src="<?=base_url('themes/assets');?>/jquery.circliful.min.js" type="text/javascript"></script>
        <script src="<?=base_url('themes/assets');?>/jquery.tosrus.min.all.js" type="text/javascript"></script>
        <script src="<?=base_url('themes/assets');?>/jquery.inputmask.bundle.js" type="text/javascript"></script>
       