<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"><style type="text/css">[uib-typeahead-popup].dropdown-menu{display:block;}</style>
      <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>e-Recruitment PT. Jakarta Prima Crane</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/icon" href="<?=base_url('G.jpg');?>">
        <link href="<?=base_url('themes/assets');?>/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url('themes/assets');?>/default-theme.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url('themes/assets');?>/style.css" rel="stylesheet" type="text/css">
        </head>
    <body>    
        <a class="scrollToTop" href="#"></a>
        <header id="header">
            <!-- BEGIN MENU -->
            <div class="menu_area">
                <nav class="navbar navbar-default navbar-fixed-top" role="navigation">  
                    <div class="container">
                        <div class="navbar-header">
                            <!-- FOR MOBILE VIEW COLLAPSED BUTTON -->
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <!-- LOGO -->
                            <!-- IMG BASED LOGO  -->
                            <a class="navbar-brand" href="<?=base_url();?>" style="margin-top: 10px;padding-top: 0px;">
                                <img class="hidden-xs" src="<?=base_url('G.jpg');?>" alt="logo" style="width: 70px;height: 70px; margin-top: -5px;">
                                <img class="visible-xs" src="<?=base_url('G.jpg');?>" alt="logo" style="width: 70px;height: 70px; margin-top: -5px;">
                            </a>

                        </div>
                        <div id="navbar" class="navbar-collapse collapse">
                            <ul id="top-menu" class="nav navbar-nav navbar-right main-nav">
                                <li class=" active "><a style="font-size: medium;" href="<?=base_url();?>"><b>Lowongan</b></a></li>
                                <li class=""><a style="font-size: medium;" href="<?=base_url('jobprofile');?>"><b>Job Profile</b></a></li>
                                <li class=""><a style="font-size: medium;" href="<?=base_url('panduan');?>"><b>Panduan</b></a></li>
                                <li class=""><a style="font-size: medium;" href="<?=base_url('registrasi');?>"><b>Registrasi</b></a></li>
                                <!-- <li class=""><a style="font-size: medium;" href="<?=base_url();?>"><b>Login</b></a></li> -->
                            </ul>           
                        </div>
                    </div>     
                <hr style="margin-top: 2px;margin-bottom: 0px;
                           border-width: thin;
                           border-style: double; border-color: #d62419;">
                </nav>
            </div>
        </header>
