    <section id="studentsTestimonial" style="margin-bottom: 0px;padding-bottom: 0px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                </div>
            </div>
        </div>
    </section>
    <section id="courseArchive">
        <div class="container">
            <div class="row">
                <!-- start course content -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="courseArchive_content">
                        <!-- start blog archive  -->
                        <div class="row">
                            <!-- start single blog archive -->


                            <div class="col-lg-12 col-12 col-sm-12">
                                <div class="single_blog_archive wow fadeInUp">
                          
                                        <br>
                                        <br>
                                        <br>
                                        <center><b>REGISTRASI</b><br>Semua isian wajib diisi.</center>
                                        <br>
                                    <div class="col-md-10">
                                    <form action="<?=base_url('registrasi/proses_reg');?>" method="POST">
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-2 col-form-label">No. KTP</label>
                                            <div class="col-sm-10">
                                            <input type="text" class="form-control" name="no_ktp" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 col-form-label">Nama Lengkap</label>
                                            <div class="col-sm-10">
                                            <input type="text" class="form-control" name="nama_lengkap" required>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 col-form-label">Tempat Kelahiran</label>
                                            <div class="col-sm-10">
                                            <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" required>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                                            <div class="col-sm-10">
                                            <input type="date" class="form-control" id="tanggal_lahir" name="tanggal_lahir" required>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 col-form-label">Email</label>
                                            <div class="col-sm-10">
                                            <input type="email" class="form-control" id="email" name="email" required>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 col-form-label">No. Telepon</label>
                                            <div class="col-sm-10">
                                            <input type="text" class="form-control" id="no_telp" name="no_telp" required>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 col-form-label">No. WhatsApp (Aktif)</label>
                                            <div class="col-sm-10">
                                            <input type="text" class="form-control" id="no_wa" name="no_wa" required>
                                            </div>
                                        </div>

                                        <?php 
                                        if(@$loker['judul_lowongan'] !== null){ ?>
                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 col-form-label">Lowongan yang dipilih</label>
                                            <div class="col-sm-10">
                                            <input type="text" class="form-control" id="lowongan" name="lowongan" value="<?=@$loker['judul_lowongan'];?>" readonly>
                                            <input type="hidden" class="form-control" id="id_lowongan" name="id_lowongan" value="<?=@$loker['id_lowongan'];?>" readonly>
                                            </div>
                                        </div>

                                        <?php }else{ ?>
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-2 col-form-label">Lowongan yang dipilih</label>
                                                <div class="col-sm-10">
                                                <select name="id_lowongan" id="id_lowongan" class="form-control" required>
                                                <option value="">-- pilih lowongan --</option>
                                                    <?php 
                                                    $lowongan = $this->db->query("SELECT * FROM lowongan WHERE stt_lowongan='1'")->result();
                                                    foreach($lowongan as $low){
                                                        echo '<option value="'.$low->id_lowongan.'">'.$low->judul_lowongan.'</option>';
                                                    }
                                                    ?>
                                                    
                                                </select>
                                                </div>
                                            </div>

                                        <?php } ?>

                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 col-form-label"></label>
                                            <div class="col-sm-10">
                                            <button type="submit" class="btn btn-primary btn-sm">Kirim Data</button>
                                            </div>
                                        </div>

                                        </form></div>
                            <br>
                            <div class="row">
                            <div class="col-md-8"></div>
                            </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>