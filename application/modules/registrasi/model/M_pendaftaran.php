<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pendaftaran extends CI_Model
{
  //DATA PRIBADI
  private $identitas_table = "identitas";
  private $data_keluarga = "data_keluarga";
  private $pendidikan_formal = "pendidikan_formal";
  private $pendidikan_nonformal = "pendidikan_non_formal";
  private $bahasa = "bahasa";
  private $prestasi = "prestasi";
  private $keahlian = "keahlian";
  private $kegiatan_hoby = "kegiatan_hoby";
  private $pengalaman_kerja = "pengalaman_kerja";
  private $atasan = "nama_atasan";
  private $uraian = "uraian";
  private $bawahan = "bawahan";
  private $data_gaji = "data_gaji";
  private $refrensi = "refrensi";
  private $lain_lain = "lain_lain";
  private $penyakit = "penyakit";

  //DATA PRIBADI
  public $id;
  public $nama_lengkap;
  public $nama_panggilan;
  public $tempat_tgl_lahir;
  public $jenis_kelamin;
  public $alamat_sesuai_ktp; 
  public $kode_pos_ktp;
  public $alamat_sekarang;
  public $kode_pos_alamat;
  public $no_telp_pribadi;
  public $no_telp_istri;
  public $agama;
  public $suku_bangsa;
  public $kewarganegaraan;
  public $status;
  public $no_ktp;
  public $masa_berlaku_ktp;
  public $no_npwp;
  public $no_bpjs;
  public $tanggal_terbit_bpjs;
  public $no_jaminan_pensiun;
  public $tanggal_terbit_jaminan;
  public $sim;
  public $masa_berlaku_sim;
  public $golongan_darah;
  public $ukuran_baju;
  public $berat_badan;
  public $ukuran_sepatu;
  public $tinggi_badan;
  public $email;

  //DATA KELUARGA
  public $id_keluarga;
  public $hubungan_keluarga;
  public $nama_keluarga;
  public $jenis_kelamin_keluarga;
  public $tanggal_lahir_keluarga;
  public $pendidikan_keluarga;
  public $pekerjaan_keluarga;

  //PENDIDIKAN FORMAL
  public $id_pendidikan_formal;
  public $sekolah;
  public $dari_tahun_formal;
  public $sampai_tahun_formal;
  public $jurusan;
  public $tempat_formal;
  public $ijazah_formal;
  public $keterangan_formal;

  //PENDIDIKAN nonFORMAL
  public $id_pendidikan_nonformal;
  public $pelatihan;
  public $tahun;
  public $penyelenggara_pelatihan;
  public $tempat_pelatihan;
  public $sertifikat_pelatihan;
  public $keterangan_pelatihan;

  //BAHASA
  public $id_bahasa;
  // public $bahasa;
  public $berbicara;
  public $membaca;
  public $menulis;
  
  //PRESTASI
  public $id_prestasi;
  public $kejuaraan;
  public $bidang;
  public $penyelenggara_kejuaraan;
  public $rengking;
  public $sertifikat_kejuaraan;

  //KEAHLIAN
  public $id_keahlian;
  public $nama_keahlian;
  public $tingkat_keahlian;
  public $sertifikat_keahlian;

  //KEGIATAN HOBY
  public $id_kegiatan_hoby;
  public $hoby;
  public $nama_organisasi;
  public $jenis_organisasi;
  public $tahun_kegiatan;
  public $jabatan_organisasi;

  //PENGALAMAN KERJA
  public $id_pengalaman_kerja;
  public $nama_perusahaan;
  public $dari_tahun_pengalaman;
  public $sampai_tahun_pengalaman;
  public $alasan_berhenti;
  public $gaji;

  public $id_nama_atasan;
  public $nama_atasan;
  public $jabatan_atasan;
  public $telpon_atasan;
  public $perusahaan_atasan;
  
  public $id_uraian;
  public $jabatan_terakhir;
  public $pencapaian;
  
  public $id_bawahan;
  public $jabatan;
  public $perusahaan;
  public $jumlah_karyawan;
  
  public $id_data_gaji;
  public $gaji_pokok;
  public $lembur;
  public $makan;
  public $bonus;
  public $kesehatan;
  public $gaji_lain;
  
  public $id_refrensi;
  public $nama_refrensi;
  public $alamat_refrensi;
  public $pekerjaan_refrensi;
  public $telpon_refrensi;
  public $tahun_kenal;
  
  //LAIN LAIN
  public $id_lain_lain;
  public $pertanyaan_1;
  public $pertanyaan_2;
  public $pertanyaan_3;
  public $pertanyaan_4;
  public $pertanyaan_5;
  public $pertanyaan_6;
  public $pertanyaan_7;
  
  public $id_penyakit;
  public $pertanyaan;
  public $pilihan;
  public $jenis_penyakit;
  public $tahun_berapa;
  public $berapa_lama;
  public $dirawat_di;

  public function rules()
  {
    return [
      [
        'field' => 'nama_lengkap',
        'label' => 'Nama lengkap',
        'rules' => 'required'
      ],

      [
        'field' => 'nama_panggilan',
        'label' => 'Nama panggilan',
        'rules' => 'required'
      ],

      [
        'field' => 'tempat_tgl_lahir',
        'label' => 'Tempat tanggal lahir',
        'rules' => 'required'
      ],
      [
        'field' => 'jenis_kelamin',
        'label' => 'Jenis kelamin',
        'rules' => 'required'
      ],
      [
        'field' => 'alamat_sesuai_ktp',
        'label' => 'Alamat sesuai ktp',
        'rules' => 'required'
      ],
      [
        'field' => 'kode_pos_ktp',
        'label' => 'Kode pos ktp',
        'rules' => 'numeric|required'
      ],
      [
        'field' => 'alamat_sekarang',
        'label' => 'Alamat sekarang',
        'rules' => 'required'
      ],
      [
        'field' => 'kode_pos_alamat',
        'label' => 'Kode pos alamat',
        'rules' => 'numeric|required'
      ],
      [
        'field' => 'no_telp_pribadi',
        'label' => 'No telp pribadi',
        'rules' => 'numeric|required'
      ],
      [
        'field' => 'no_telp_istri',
        'label' => 'No telp istri',
        'rules' => 'numeric'
      ],
      [
        'field' => 'agama',
        'label' => 'Agama',
        'rules' => 'required'
      ],
      [
        'field' => 'suku_bangsa',
        'label' => 'Suku bangsa',
        'rules' => 'required'
      ],
      [
        'field' => 'kewarganegaraan',
        'label' => 'Kewarganegaraan',
        'rules' => 'required'
      ],
      [
        'field' => 'status',
        'label' => 'status',
        'rules' => 'required'
      ],
      [
        'field' => 'no_ktp',
        'label' => 'No ktp',
        'rules' => 'numeric|required'
      ],
      [
        'field' => 'masa_berlaku_ktp',
        'label' => 'Masa berlaku ktp',
        'rules' => 'required'
      ],
      [
        'field' => 'no_npwp',
        'label' => 'No npwp',
        'rules' => 'numeric|required'
      ],
      [
        'field' => 'no_bpjs',
        'label' => 'No bpjs',
        'rules' => 'numeric|required'
      ],
      [
        'field' => 'tanggal_terbit_bpjs',
        'label' => 'Tanggal terbit bpjs',
        'rules' => 'required'
      ],
      [
        'field' => 'no_jaminan_pensiun',
        'label' => 'No jaminan pensiun',
        'rules' => 'numeric|required'
      ],
      [
        'field' => 'tanggal_terbit_jaminan',
        'label' => 'Tanggal terbit jaminan',
        'rules' => 'required'
      ],
      [
        'field' => 'sim',
        'label' => 'Sim',
        'rules' => 'numeric|required'
      ],
      [
        'field' => 'masa_berlaku_sim',
        'label' => 'Masa berlaku sim',
        'rules' => 'required'
      ],
      [
        'field' => 'golongan_darah',
        'label' => 'Golongan darah',
        'rules' => 'required'
      ],
      [
        'field' => 'ukuran_baju',
        'label' => 'Ukuran baju',
        'rules' => 'required'
      ],
      [
        'field' => 'berat_badan',
        'label' => 'Berat badan',
        'rules' => 'numeric|required'
      ],
      [
        'field' => 'ukuran_sepatu',
        'label' => 'Ukuran sepatu',
        'rules' => 'numeric|required'
      ],
      [
        'field' => 'tinggi_badan',
        'label' => 'Tinggi badan',
        'rules' => 'numeric|required'
      ],
      [
        'field' => 'email',
        'label' => 'email',
        'rules' => 'required|valid_email'
      ],

      //DATA KELUARGA
      [
        'field' => 'nama_keluarga',
        'label' => 'Nama',
        'rules' => 'required'
      ],
      [
        'field' => 'jenis_kelamin_keluarga',
        'label' => 'Jenis kelamin',
        'rules' => 'required'
      ],
      [
        'field' => 'tanggal_lahir_keluarga',
        'label' => 'Tanggal Lahir',
        'rules' => 'required'
      ],
      [
        'field' => 'pendidikan_keluarga',
        'label' => 'Pendidikan',
        'rules' => 'required'
      ],
      [
        'field' => 'pekerjaan_keluarga',
        'label' => 'Pekerjaan',
        'rules' => 'required'
      ],

      //PENDIDIKAN FORMAL
    ];
  }

  public function save()
  {
    $this->db->trans_start();

    $post = $this->input->post();
    
    //DATA PRIBADI
    $data_pribadi = [
      'nama_lengkap' => $post["nama_lengkap"],
      'nama_panggilan' => $post["nama_panggilan"],
      'tempat_tgl_lahir' => $post["tempat_tgl_lahir"],
      'jenis_kelamin' => $post["jenis_kelamin"],
      'alamat_sesuai_ktp' => $post["alamat_sesuai_ktp"],
      'kode_pos_ktp' => $post["kode_pos_ktp"],
      'alamat_sesuai_ktp' => $post["alamat_sesuai_ktp"],
      'kode_pos_ktp' => $post["kode_pos_ktp"],
      'alamat_sekarang' => $post["alamat_sekarang"],
      'kode_pos_alamat' => $post["kode_pos_alamat"],
      'no_telp_pribadi' => $post["no_telp_pribadi"],
      'no_telp_istri' => $post["no_telp_istri"],
      'agama' => $post["agama"],
      'suku_bangsa' => $post["suku_bangsa"],
      'kewarganegaraan' => $post["kewarganegaraan"],
      'status' => $post["status"],
      'no_ktp' => $post["no_ktp"],
      'masa_berlaku_ktp' => $post["masa_berlaku_ktp"],
      'no_npwp' => $post["no_npwp"],
      'no_bpjs' => $post["no_bpjs"],
      'tanggal_terbit_bpjs' => $post["tanggal_terbit_bpjs"],
      'no_jaminan_pensiun' => $post["no_jaminan_pensiun"],
      'tanggal_terbit_bpjs' => $post["tanggal_terbit_bpjs"],
      'no_jaminan_pensiun' => $post["no_jaminan_pensiun"],
      'tanggal_terbit_bpjs' => $post["tanggal_terbit_bpjs"],
      'no_jaminan_pensiun' => $post["no_jaminan_pensiun"],
      'tanggal_terbit_jaminan' => $post["tanggal_terbit_jaminan"],
      'sim' => $post["sim"],
      'masa_berlaku_sim' => $post["masa_berlaku_sim"],
      'golongan_darah' => $post["golongan_darah"],
      'ukuran_baju' => $post["ukuran_baju"],
      'berat_badan' => $post["berat_badan"],
      'ukuran_sepatu' => $post["ukuran_sepatu"],
      'tinggi_badan' => $post["tinggi_badan"],
      'email' => $post["email"]
    ];
    return $this->db->insert($this->identitas_table, $data_pribadi);

    $last_id = $this->db->insert_id();

    //DATA KELUARGA
    $data_ayah = [
      'id_identitas' => $last_id,
      'hubungan_keluarga' => 'ayah',
      'nama_keluarga' => $post["nama_ayah"],
      'jenis_kelamin_keluarga' => $post["jenis_kelamin_ayah"],
      'tanggal_lahir_keluarga' => $post["tanggal_lahir_ayah"],
      'pendidikan_keluarga' => $post["pendidikan_ayah"],
      'pekerjaan_keluarga' => $post["pekerjaan_ayah"],
    ];
    return $this->db->insert($this->data_keluarga, $data_ayah);

    $data_ibu = [
      'id_identitas' => $last_id,
      'hubungan_keluarga' => 'ibu',
      'nama_keluarga' => $post["nama_ibu"],
      'jenis_kelamin_keluarga' => $post["jenis_kelamin_ibu"],
      'tanggal_lahir_keluarga' => $post["tanggal_lahir_ibu"],
      'pendidikan_keluarga' => $post["pendidikan_ibu"],
      'pekerjaan_keluarga' => $post["pekerjaan_ibu"],
    ];
    return $this->db->insert($this->data_keluarga, $data_ibu);

    $data_saudara1 = [
      'id_identitas' => $last_id,
      'hubungan_keluarga' => 'saudara 1',
      'nama_keluarga' => $post["nama_saudara1"],
      'jenis_kelamin_keluarga' => $post["jenis_kelamin_saudara1"],
      'tanggal_lahir_keluarga' => $post["tanggal_lahir_saudara1"],
      'pendidikan_keluarga' => $post["pendidikan_saudara1"],
      'pekerjaan_keluarga' => $post["pekerjaan_saudara1"],
    ];
    return $this->db->insert($this->data_keluarga, $data_saudara1);

    $data_saudara2 = [
      'id_identitas' => $last_id,
      'hubungan_keluarga' => 'saudara 2',
      'nama_keluarga' => $post["nama_saudara2"],
      'jenis_kelamin_keluarga' => $post["jenis_kelamin_saudara2"],
      'tanggal_lahir_keluarga' => $post["tanggal_lahir_saudara2"],
      'pendidikan_keluarga' => $post["pendidikan_saudara2"],
      'pekerjaan_keluarga' => $post["pekerjaan_saudara2"],
    ];
    return $this->db->insert($this->data_keluarga, $data_saudara2);

    $data_saudara3 = [
      'id_identitas' => $last_id,
      'hubungan_keluarga' => 'saudara 3',
      'nama_keluarga' => $post["nama_saudara3"],
      'jenis_kelamin_keluarga' => $post["jenis_kelamin_saudara3"],
      'tanggal_lahir_keluarga' => $post["tanggal_lahir_saudara3"],
      'pendidikan_keluarga' => $post["pendidikan_saudara3"],
      'pekerjaan_keluarga' => $post["pekerjaan_saudara3"],
    ];
    return $this->db->insert($this->data_keluarga, $data_saudara3);

    $data_saudara4 = [
      'id_identitas' => $last_id,
      'hubungan_keluarga' => 'saudara 4',
      'nama_keluarga' => $post["nama_saudara4"],
      'jenis_kelamin_keluarga' => $post["jenis_kelamin_saudara4"],
      'tanggal_lahir_keluarga' => $post["tanggal_lahir_saudara4"],
      'pendidikan_keluarga' => $post["pendidikan_saudara4"],
      'pekerjaan_keluarga' => $post["pekerjaan_saudara4"],
    ];
    return $this->db->insert($this->data_keluarga, $data_saudara4);


    $data_suami = [
      'id_identitas' => $last_id,
      'hubungan_keluarga' => 'suami',
      'nama_keluarga' => $post["nama_suami"],
      'jenis_kelamin_keluarga' => $post["jenis_kelamin_suami"],
      'tanggal_lahir_keluarga' => $post["tanggal_lahir_suami"],
      'pendidikan_keluarga' => $post["pendidikan_suami"],
      'pekerjaan_keluarga' => $post["pekerjaan_suami"],
    ];
    return $this->db->insert($this->data_keluarga, $data_suami);

    $data_istri = [
      'id_identitas' => $last_id,
      'hubungan_keluarga' => 'istri',
      'nama_keluarga' => $post["nama_istri"],
      'jenis_kelamin_keluarga' => $post["jenis_kelamin_istri"],
      'tanggal_lahir_keluarga' => $post["tanggal_lahir_istri"],
      'pendidikan_keluarga' => $post["pendidikan_istri"],
      'pekerjaan_keluarga' => $post["pekerjaan_istri"],
    ];
    return $this->db->insert($this->data_keluarga, $data_istri);

    $data_anak1 = [
      'id_identitas' => $last_id,
      'hubungan_keluarga' => 'anak 1',
      'nama_keluarga' => $post["nama_anak1"],
      'jenis_kelamin_keluarga' => $post["jenis_kelamin_anak1"],
      'tanggal_lahir_keluarga' => $post["tanggal_lahir_anak1"],
      'pendidikan_keluarga' => $post["pendidikan_anak1"],
      'pekerjaan_keluarga' => $post["pekerjaan_anak1"],
    ];
    return $this->db->insert($this->data_keluarga, $data_anak1);

    $data_anak2 = [
      'id_identitas' => $last_id,
      'hubungan_keluarga' => 'anak 2',
      'nama_keluarga' => $post["nama_anak2"],
      'jenis_kelamin_keluarga' => $post["jenis_kelamin_anak2"],
      'tanggal_lahir_keluarga' => $post["tanggal_lahir_anak2"],
      'pendidikan_keluarga' => $post["pendidikan_anak2"],
      'pekerjaan_keluarga' => $post["pekerjaan_anak2"],
    ];
    return $this->db->insert($this->data_keluarga, $data_anak2);

    $data_anak3 = [
      'id_identitas' => $last_id,
      'hubungan_keluarga' => 'anak 3',
      'nama_keluarga' => $post["nama_anak3"],
      'jenis_kelamin_keluarga' => $post["jenis_kelamin_anak3"],
      'tanggal_lahir_keluarga' => $post["tanggal_lahir_anak3"],
      'pendidikan_keluarga' => $post["pendidikan_anak3"],
      'pekerjaan_keluarga' => $post["pekerjaan_anak3"],
    ];
    return $this->db->insert($this->data_keluarga, $data_anak3);


    //PENDIDIKAN FORMAL
    $pendidikan_formal = [
      'id_identitas' => $last_id,
      'sekolah' => ["sekolah"],
      'dari_tahun' => ["dari_tahun_formal"],
      'sampai_tahun' => ["sampai_tahun_formal"],
      'jurusan' => ["jurusan"],
      'tempat' => ["tempat_formal"],
      'ijazah' => ["ijazah_formal"],
      'keterangan' => ["keterangan_formal"],
    ];
    return $this->db->insert($this->pendidikan_formal, $pendidikan_formal);

    $pendidikan_formal2 = [
      'id_identitas' => $last_id,
      'sekolah' => ["sekolah2"],
      'dari_tahun' => ["dari_tahun_formal2"],
      'sampai_tahun' => ["sampai_tahun_formal2"],
      'jurusan' => ["jurusan2"],
      'tempat' => ["tempat_formal2"],
      'ijazah' => ["ijazah_formal2"],
      'keterangan' => ["keterangan_formal2"],
    ];
    return $this->db->insert($this->pendidikan_formal, $pendidikan_formal2);

    $pendidikan_formal3 = [
      'id_identitas' => $last_id,
      'sekolah' => ["sekolah3"],
      'dari_tahun' => ["dari_tahun_formal3"],
      'sampai_tahun' => ["sampai_tahun_formal3"],
      'jurusan' => ["jurusan3"],
      'tempat' => ["tempat_formal3"],
      'ijazah' => ["ijazah_formal3"],
      'keterangan' => ["keterangan_formal3"],
    ];
    return $this->db->insert($this->pendidikan_formal, $pendidikan_formal3);

    $pendidikan_formal4 = [
      'id_identitas' => $last_id,
      'sekolah' => ["sekolah4"],
      'dari_tahun' => ["dari_tahun_formal4"],
      'sampai_tahun' => ["sampai_tahun_formal4"],
      'jurusan' => ["jurusan4"],
      'tempat' => ["tempat_formal4"],
      'ijazah' => ["ijazah_formal4"],
      'keterangan' => ["keterangan_formal4"],
    ];
    return $this->db->insert($this->pendidikan_formal, $pendidikan_formal4);


    //PENDIDIKAN NON FORMAL
    $pendidikan_nonformal = [
      'id_identitas' => $last_id,
      'pelatihan' => ["pelatihan"],
      'tahun' => ["tahun_pelatihan"],
      'penyelenggara' => ["penyelenggara_pelatihan"],
      'tempat' => ["tempat_pelatihan"],
      'sertifikat' => ["sertifikat_pelatihan"],
      'keterangan' => ["keterangan_pelatihan"],
    ];
    return $this->db->insert($this->pendidikan_nonformal, $pendidikan_nonformal);

    $pendidikan_nonformal2 = [
      'id_identitas' => $last_id,
      'pelatihan' => ["pelatihan2"],
      'tahun' => ["tahun_pelatihan2"],
      'penyelenggara' => ["penyelenggara_pelatihan2"],
      'tempat' => ["tempat_pelatihan2"],
      'sertifikat' => ["sertifikat_pelatihan2"],
      'keterangan' => ["keterangan_pelatihan2"],
    ];
    return $this->db->insert($this->pendidikan_nonformal, $pendidikan_nonformal2);

    $pendidikan_nonformal3 = [
      'id_identitas' => $last_id,
      'pelatihan' => ["pelatihan3"],
      'tahun' => ["tahun_pelatihan3"],
      'penyelenggara' => ["penyelenggara_pelatihan3"],
      'tempat' => ["tempat_pelatihan3"],
      'sertifikat' => ["sertifikat_pelatihan3"],
      'keterangan' => ["keterangan_pelatihan3"],
    ];
    return $this->db->insert($this->pendidikan_nonformal, $pendidikan_nonformal3);


    //BAHASA
    $bahasa = [
      'id_identitas' => $last_id,
      'bahasa' => ["bahasa"],
      'berbicara' => ["berbicara"],
      'membaca' => ["membaca"],
      'menulis' => ["menulis"],
    ];
    return $this->db->insert($this->bahasa, $bahasa);

    $bahasa2 = [
      'id_identitas' => $last_id,
      'bahasa' => ["bahasa2"],
      'berbicara' => ["berbicara2"],
      'membaca' => ["membaca2"],
      'menulis' => ["menulis2"],
    ];
    return $this->db->insert($this->bahasa, $bahasa2);

    $bahasa3 = [
      'id_identitas' => $last_id,
      'bahasa' => ["bahasa3"],
      'berbicara' => ["berbicara3"],
      'membaca' => ["membaca3"],
      'menulis' => ["menulis3"],
    ];
    return $this->db->insert($this->bahasa, $bahasa3);


    //PRESTASI
    $prestasi = [
      'id_identitas' => $last_id,
      'kejuaraan' => ["kejuaraan"],
      'bidang' => ["bidang"],
      'penyelenggara' => ["penyelenggara_kejuaraan"],
      'rengking' => ["rengking"],
      'sertifikat' => ["sertifikat_kejuaraan"],
    ];
    return $this->db->insert($this->prestasi, $prestasi);

    $prestasi2 = [
      'id_identitas' => $last_id,
      'kejuaraan' => ["kejuaraan2"],
      'bidang' => ["bidang2"],
      'penyelenggara' => ["penyelenggara_kejuaraan2"],
      'rengking' => ["rengking2"],
      'sertifikat' => ["sertifikat_kejuaraan2"],
    ];
    return $this->db->insert($this->prestasi, $prestasi2);
    
    $prestasi3 = [
      'id_identitas' => $last_id,
      'kejuaraan' => ["kejuaraan3"],
      'bidang' => ["bidang3"],
      'penyelenggara' => ["penyelenggara_kejuaraan3"],
      'rengking' => ["rengking3"],
      'sertifikat' => ["sertifikat_kejuaraan3"],
    ];
    return $this->db->insert($this->prestasi, $prestasi3);


    //KEAHLIAN
    $keahlian = [
      'id_identitas' => $last_id,
      'nama_keahlian' => ["nama_keahlian"],
      'tingkat_keahlian' => ["tingkat_keahlian"],
      'sertifikat' => ["sertifikat_keahlian"],
    ];
    return $this->db->insert($this->keahlian, $keahlian);

    $keahlian2 = [
      'id_identitas' => $last_id,
      'nama_keahlian' => ["nama_keahlian2"],
      'tingkat_keahlian' => ["tingkat_keahlian2"],
      'sertifikat' => ["sertifikat_keahlian2"],
    ];
    return $this->db->insert($this->keahlian, $keahlian2);

    $keahlian3 = [
      'id_identitas' => $last_id,
      'nama_keahlian' => ["nama_keahlian3"],
      'tingkat_keahlian' => ["tingkat_keahlian3"],
      'sertifikat' => ["sertifikat_keahlian3"],
    ];
    return $this->db->insert($this->keahlian, $keahlian3);


    //KEGIATAN HOBY
    $kegiatan = [
      'id_identitas' => $last_id,
      'hoby' => ["hoby"],
      'nama_organisasi' => ["nama_organisasi"],
      'jenis_organisasi' => ["jenis_organisasi"],
      'tahun' => ["tahun_kegiatan"],
      'jabatan' => ["jabatan_organisasi"],
    ];
    return $this->db->insert($this->kegiatan_hoby, $kegiatan);


    //PENGALAMAN KERJA
    $pengalaman_kerja = [
      'id_identitas' => $last_id,
      'nama_perusahaan' => ["nama_perusahaan"],
      'dari_tahun' => ["dari_tahun_pengalaman"],
      'sampai_tahun' => ["sampai_tahun_pengalaman"],
      'alasan_berhenti' => ["alasan_berhenti"],
      'gaji' => ["gaji"],
    ];
    return $this->db->insert($this->pengalaman_kerja, $pengalaman_kerja);

    $pengalaman_kerja2 = [
      'id_identitas' => $last_id,
      'nama_perusahaan' => ["nama_perusahaan2"],
      'dari_tahun' => ["dari_tahun_pengalaman2"],
      'sampai_tahun' => ["sampai_tahun_pengalaman2"],
      'alasan_berhenti' => ["alasan_berhenti2"],
      'gaji' => ["gaji"],
    ];
    return $this->db->insert($this->pengalaman_kerja, $pengalaman_kerja2);

    $pengalaman_kerja3 = [
      'id_identitas' => $last_id,
      'nama_perusahaan' => ["nama_perusahaan3"],
      'dari_tahun' => ["dari_tahun_pengalaman3"],
      'sampai_tahun' => ["sampai_tahun_pengalaman3"],
      'alasan_berhenti' => ["alasan_berhenti3"],
      'gaji' => ["gaji"],
    ];
    return $this->db->insert($this->pengalaman_kerja, $pengalaman_kerja3);

    //ATASAN
    $atasan = [
      'id_identitas' => $last_id,
      'nama' => ["nama_atasan"],
      'jabatan' => ["jabatan_atasan"],
      'telpon' => ["telpon_atasan"],
      'perusahaan' => ["perusahaan_atasan"],
    ];
    return $this->db->insert($this->atasan, $atasan);

    $atasan2 = [
      'id_identitas' => $last_id,
      'nama' => ["nama_atasan2"],
      'jabatan' => ["jabatan_atasan2"],
      'telpon' => ["telpon_atasan2"],
      'perusahaan' => ["perusahaan_atasan2"],
    ];
    return $this->db->insert($this->atasan, $atasan2);

    $atasan3 = [
      'id_identitas' => $last_id,
      'nama' => ["nama_atasan3"],
      'jabatan' => ["jabatan_atasan3"],
      'telpon' => ["telpon_atasan3"],
      'perusahaan' => ["perusahaan_atasan3"],
    ];
    return $this->db->insert($this->atasan, $atasan3);

    //URAIAN
    $uraian = [
      'id_identitas' => $last_id,
      'jabatan_terakhir' => ["jabatan_terakhir"],
      'pencapaian' => ["pencapaian"],
    ];
    return $this->db->insert($this->uraian, $uraian);

    //BAWAHAN
    $bawahan = [
      'id_identitas' => $last_id,
      'jabatan' => ["jabatan"],
      'perusahaan' => ["perusahaan"],
      'jumlah_karyawan' => ["jumlah_karyawan"],
    ];
    return $this->db->insert($this->bawahan, $bawahan);

    $bawahan2 = [
      'id_identitas' => $last_id,
      'jabatan' => ["jabatan2"],
      'perusahaan' => ["perusahaan2"],
      'jumlah_karyawan' => ["jumlah_karyawan2"],
    ];
    return $this->db->insert($this->bawahan, $bawahan2);

    $bawahan3 = [
      'id_identitas' => $last_id,
      'jabatan' => ["jabatan3"],
      'perusahaan' => ["perusahaan3"],
      'jumlah_karyawan' => ["jumlah_karyawan3"],
    ];
    return $this->db->insert($this->bawahan, $bawahan3);

    //DATA GAJI
    $data_gaji = [
      'id_identitas' => $last_id,
      'gaji_pokok' => ["gaji_pokok"],
      'lembur' => ["lembur"],
      'makan' => ["makan"],
      'bonus' => ["bonus"],
      'kesehatan' => ["kesehatan"],
      'gaji_lain' => ["gaji_lain"],
    ];
    return $this->db->insert($this->data_gaji, $data_gaji);

    //REFRENSI
    $refrensi = [
      'id_identitas' => $last_id,
      'nama' => ["nama_refrensi"],
      'alamat' => ["alamat_refrensi"],
      'pekerjaan' => ["pekerjaan_refrensi"],
      'telpon' => ["telpon_refrensi"],
      'tahun_kenal' => ["tahun_kenal"],
    ];
    return $this->db->insert($this->refrensi, $refrensi);

    $refrensi2 = [
      'id_identitas' => $last_id,
      'nama' => ["nama_refrensi2"],
      'alamat' => ["alamat_refrensi2"],
      'pekerjaan' => ["pekerjaan_refrensi2"],
      'telpon' => ["telpon_refrensi2"],
      'tahun_kenal' => ["tahun_kenal2"],
    ];
    return $this->db->insert($this->refrensi, $refrensi2);

    $refrensi3 = [
      'id_identitas' => $last_id,
      'nama' => ["nama_refrensi3"],
      'alamat' => ["alamat_refrensi3"],
      'pekerjaan' => ["pekerjaan_refrensi3"],
      'telpon' => ["telpon_refrensi3"],
      'tahun_kenal' => ["tahun_kenal3"],
    ];
    return $this->db->insert($this->refrensi, $refrensi3);


    //LAIN LAIN
    $lain_lain = [
      'id_identitas' => $last_id,
      'pertanyaan_1' => ["pertanyaan_1"],
      'pertanyaan_2' => ["pertanyaan_2"],
      'pertanyaan_3' => ["pertanyaan_3"],
      'pertanyaan_4' => ["pertanyaan_4"],
      'pertanyaan_5' => ["pertanyaan_5"],
      'pertanyaan_6' => ["pertanyaan_6"],
      'pertanyaan_7' => ["pertanyaan_7"],
    ];
    return $this->db->insert($this->lain_lain, $lain_lain);

    //PENYAKIT
    $penyakit = [
      'id_identitas' => $last_id,
      'pertanyaan' => "Pernahkah anda menderita penyakit berat, sampai dirawat di rumah sakit atau menderita penyakit yang lama sembuh?",
      'pilihan' => ["pilihan"],
      'jenis_penyakit' => ["jenis_penyakit"],
      'tahun_berapa' => ["tahun_berapa"],
      'berapa_lama' => ["berapa_lama"],
      'dirawat_di' => ["dirawat_di"],
    ];
    return $this->db->insert($this->penyakit, $penyakit);

    $penyakit2 = [
      'id_identitas' => $last_id,
      'pertanyaan' => "Apakah anda pernah menderita penyakit yang menyebabkan hilang nya kesadaran secara tiba tiba?",
      'pilihan' => ["pilihan2"],
      'jenis_penyakit' => ["jenis_penyakit2"],
      'tahun_berapa' => ["tahun_berapa2"],
      'berapa_lama' => ["berapa_lama2"],
      'dirawat_di' => ["dirawat_di2"],
    ];
    return $this->db->insert($this->penyakit, $penyakit2);
    

    $this->db->trans_complete();
  }
}