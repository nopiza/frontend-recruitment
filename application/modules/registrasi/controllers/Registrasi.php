<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrasi extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->helper('customade');
		$this->load->model("M_pendaftaran");
		$this->load->library(array('form_validation'));	
		// check_login();

	}
	
	public function index()
	{
		$data=array();
		$csrf = array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
		    );
		$data=array('csrf'=>$csrf);	

		$this->load->view('template/header_kosong',$data);
		$this->load->view('form',$data);
		$this->load->view('template/footer_kosong',$data);
	}

	public function add()
	{
		$post = $this->input->post();
		$data_pribadi = [
			'nama_lengkap' => $post["nama_lengkap"],
			'nama_panggilan' => $post["nama_panggilan"],
			'tempat_tgl_lahir' => $post["tempat_tgl_lahir"],
			'jenis_kelamin' => $post["jenis_kelamin"],
			'alamat_sesuai_ktp' => $post["alamat_sesuai_ktp"],
			'kode_pos_ktp' => $post["kode_pos_ktp"],
			'alamat_sesuai_ktp' => $post["alamat_sesuai_ktp"],
			'kode_pos_ktp' => $post["kode_pos_ktp"],
			'alamat_sekarang' => $post["alamat_sekarang"],
			'kode_pos_alamat' => $post["kode_pos_alamat"],
			'no_telp_pribadi' => $post["no_telp_pribadi"],
			'no_telp_istri' => $post["no_telp_istri"],
			'agama' => $post["agama"],
			'suku_bangsa' => $post["suku_bangsa"],
			'kewarganegaraan' => $post["kewarganegaraan"],
			'status' => $post["status"],
			'no_ktp' => $post["no_ktp"],
			'masa_berlaku_ktp' => $post["masa_berlaku_ktp"],
			'no_npwp' => $post["no_npwp"],
			'no_bpjs' => $post["no_bpjs"],
			'tanggal_terbit_bpjs' => $post["tanggal_terbit_bpjs"],
			'no_jaminan_pensiun' => $post["no_jaminan_pensiun"],
			'tanggal_terbit_bpjs' => $post["tanggal_terbit_bpjs"],
			'no_jaminan_pensiun' => $post["no_jaminan_pensiun"],
			'tanggal_terbit_bpjs' => $post["tanggal_terbit_bpjs"],
			'no_jaminan_pensiun' => $post["no_jaminan_pensiun"],
			'tanggal_terbit_jaminan' => $post["tanggal_terbit_jaminan"],
			'sim' => $post["sim"],
			'masa_berlaku_sim' => $post["masa_berlaku_sim"],
			'golongan_darah' => $post["golongan_darah"],
			'ukuran_baju' => $post["ukuran_baju"],
			'berat_badan' => $post["berat_badan"],
			'ukuran_sepatu' => $post["ukuran_sepatu"],
			'tinggi_badan' => $post["tinggi_badan"],
			'email' => $post["email"]
		];

		$this->db->insert('identitas', $data_pribadi);
	}


	

}