<div class="content">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          Registrasi
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->


        <div class="row">
          <div class="col-lg-12 col-12">
            <hr>
            <br>

            <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">bs-stepper</h3>
              </div>
              <div class="card-body p-0">
                <div class="bs-stepper linear">
                  <div class="bs-stepper-header" role="tablist">
                    <!-- your steps here -->
                    <div class="step active" data-target="#pribadi-part">
                      <button type="button" class="step-trigger" role="tab" aria-controls="pribadi-part" id="pribadi-part-trigger" aria-selected="true">
                        <span class="bs-stepper-circle">1</span>
                        <span class="bs-stepper-label">Data Pribadi</span>
                      </button>
                    </div>

                    <div class="line"></div>
                    <div class="step" data-target="#keluarga-part">
                      <button type="button" class="step-trigger" role="tab" aria-controls="keluarga-part" id="keluarga-part-trigger" aria-selected="false" disabled="disabled">
                        <span class="bs-stepper-circle">2</span>
                        <span class="bs-stepper-label">Data Keluarga</span>
                      </button>
                    </div>

                    <div class="line"></div>
                    <div class="step" data-target="#pendidikan-part">
                      <button type="button" class="step-trigger" role="tab" aria-controls="pendidikan-part" id="pendidikan-part-trigger" aria-selected="false" disabled="disabled">
                        <span class="bs-stepper-circle">3</span>
                        <span class="bs-stepper-label">Data Pendidikan</span>
                      </button>
                    </div>

                    <div class="line"></div>
                    <div class="step" data-target="#nonformal-part">
                      <button type="button" class="step-trigger" role="tab" aria-controls="nonformal-part" id="nonformal-part-trigger" aria-selected="false" disabled="disabled">
                        <span class="bs-stepper-circle">4</span>
                        <span class="bs-stepper-label">Pendidikan Non Formal</span>
                      </button>
                    </div>

                    <div class="line"></div>
                    <div class="step" data-target="#bahasa-part">
                      <button type="button" class="step-trigger" role="tab" aria-controls="bahasa-part" id="bahasa-part-trigger" aria-selected="false" disabled="disabled">
                        <span class="bs-stepper-circle">5</span>
                        <span class="bs-stepper-label">Bahasa</span>
                      </button>
                    </div>

                    <div class="line"></div>
                    <div class="step" data-target="#prestasi-part">
                      <button type="button" class="step-trigger" role="tab" aria-controls="prestasi-part" id="prestasi-part-trigger" aria-selected="false" disabled="disabled">
                        <span class="bs-stepper-circle">6</span>
                        <span class="bs-stepper-label">Prestasi</span>
                      </button>
                    </div>

                    <div class="line"></div>
                    <div class="step" data-target="#keahlian-part">
                      <button type="button" class="step-trigger" role="tab" aria-controls="keahlian-part" id="keahlian-part-trigger" aria-selected="false" disabled="disabled">
                        <span class="bs-stepper-circle">7</span>
                        <span class="bs-stepper-label">Keahlian</span>
                      </button>
                    </div>

                    <div class="line"></div>
                    <div class="step" data-target="#hobby-part">
                      <button type="button" class="step-trigger" role="tab" aria-controls="hobby-part" id="hobby-part-trigger" aria-selected="false" disabled="disabled">
                        <span class="bs-stepper-circle">8</span>
                        <span class="bs-stepper-label">Kegiatan / Hoby</span>
                      </button>
                    </div>

                    <div class="line"></div>
                    <div class="step" data-target="#pengalaman-part">
                      <button type="button" class="step-trigger" role="tab" aria-controls="pengalaman-part" id="pengalaman-part-trigger" aria-selected="false" disabled="disabled">
                        <span class="bs-stepper-circle">9</span>
                        <span class="bs-stepper-label">Pengalaman Kerja</span>
                      </button>
                    </div>

                    <div class="line"></div>
                    <div class="step" data-target="#lainlain-part">
                      <button type="button" class="step-trigger" role="tab" aria-controls="lainlain-part" id="lainlain-part-trigger" aria-selected="false" disabled="disabled">
                        <span class="bs-stepper-circle">10</span>
                        <span class="bs-stepper-label">Lain - Lain</span>
                      </button>
                    </div>


                  </div>
                  <form action="<?= base_url('/registrasi/add') ?>" method="post">
                    <div class="bs-stepper-content">
                      <hr>
                      <!-- your steps content here -->
                      <div id="pribadi-part" class="content active dstepper-block" role="tabpanel" aria-labelledby="pribadi-part-trigger">
                        <label for="nama_lengkap">Nama</label>
                        <div class="form-group d-flex">
                          <input type="text" class="form-control mr-2" id="nama_lengkap" name="nama_lengkap" placeholder="Nama lengkap">
                          <input type="text" class="form-control" id="nama_panggilan" name="nama_panggilan" placeholder="Nama panggilan">
                        </div>
                        <div class="form-group">
                          <label for="tempat_tgl_lahir">Tempat tanggal lahir</label>
                          <input type="text" class="form-control" id="tempat_tgl_lahir" name="tempat_tgl_lahir" placeholder="Tempat tanggal lahir">
                        </div>
                        <div class="form-group">
                          <label for="jenis_kelamin">Jenis kelamin</label>
                          <select class="form-control" id="jenis_kelamin" name="jenis_kelamin" aria-label="Default select example">
                            <option selected>Select</option>
                            <option value="laki">Laki-laki</option>
                            <option value="perempuan">Perempuan</option>
                          </select>
                        </div>

                        <label for="alamat_sesuai_ktp">Alamat sesuai KTP</label>
                        <div class="form-group d-flex">
                          <input type="text" class="form-control w-75 mr-2" id="alamat_sesuai_ktp" name="alamat_sesuai_ktp" placeholder="Alamat sesuai KTP">
                          <input type="number" class="form-control w-25" id="kode_pos_ktp" name="kode_pos_ktp" placeholder="Kode pos KTP">
                        </div>

                        <label for="alamat_sekarang">Alamat sekarang</label>
                        <div class="form-group d-flex">
                          <input type="text" class="form-control w-75 mr-2" id="alamat_sekarang" name="alamat_sekarang" placeholder="Alamat sekarang">
                          <input type="number" class="form-control w-25" id="kode_pos_alamat" name="kode_pos_alamat" placeholder="Kode pos alamat">
                        </div>
                        <div class="form-group">
                          <label for="no_telp_pribadi">No telp pribadi</label>
                          <input type="number" class="form-control" id="no_telp_pribadi" name="no_telp_pribadi" placeholder="No telp pribadi">
                        </div>
                        <div class="form-group">
                          <label for="no_telp_istri">No telp istri</label>
                          <input type="number" class="form-control" id="no_telp_istri" name="no_telp_istri" placeholder="No telp istri">
                        </div>
                        <div class="form-group">
                          <label for="agama">Agama</label>
                          <input type="text" class="form-control" id="agama" name="agama" placeholder="Agama">
                        </div>
                        <div class="form-group">
                          <label for="suku_bangsa">Suku bangsa</label>
                          <input type="text" class="form-control" id="suku_bangsa" name="suku_bangsa" placeholder="Suku bangsa">
                        </div>
                        <div class="form-group">
                          <label for="kewarganegaraan">Kewarganegaraan</label>
                          <input type="text" class="form-control" id="kewarganegaraan" name="kewarganegaraan" placeholder="Kewarganegaraan">
                        </div>
                        <div class="form-group">
                          <label for="status">Status *inputENUM</label>
                          <select class="form-control" id="status" name="status" aria-label="Default select example">
                            <option selected>Select</option>
                            <option value="menikah">Menikah</option>
                            <option value="belum">Belum Menikah</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="no_ktp">No KTP</label>
                          <input type="number" class="form-control" id="no_ktp" name="no_ktp" placeholder="No KTP">
                        </div>
                        <div class="form-group">
                          <label for="masa_berlaku_ktp">Masa berlaku KTP</label>
                          <input type="text" class="form-control" id="masa_berlaku_ktp" name="masa_berlaku_ktp" placeholder="Masa berlaku KTP">
                        </div>
                        <div class="form-group">
                          <label for="no_npwp">No NPWP</label>
                          <input type="number" class="form-control" id="no_npwp" name="no_npwp" placeholder="No NPWP">
                        </div>
                        <div class="form-group">
                          <label for="no_bpjs">No BPJS</label>
                          <input type="number" class="form-control" id="no_bpjs" name="no_bpjs" placeholder="No BPJS">
                        </div>
                        <div class="form-group">
                          <label for="tanggal_terbit_bpjs">Tanggal terbit BPJS</label>
                          <input type="date" class="form-control" id="tanggal_terbit_bpjs" name="tanggal_terbit_bpjs" placeholder="Tanggal terbit BPJS">
                        </div>
                        <div class="form-group">
                          <label for="no_jaminan_pensiun">No jaminan pensiun</label>
                          <input type="number" class="form-control" id="no_jaminan_pensiun" name="no_jaminan_pensiun" placeholder="No jaminan pensiun">
                        </div>
                        <div class="form-group">
                          <label for="tanggal_terbit_jaminan">Tanggal terbit jaminan</label>
                          <input type="date" class="form-control" id="tanggal_terbit_jaminan" name="tanggal_terbit_jaminan" placeholder="Tanggal terbit jaminan">
                        </div>
                        <div class="form-group">
                          <label for="sim">SIM</label>
                          <input type="text" class="form-control" id="sim" name="sim" placeholder="SIM">
                        </div>
                        <div class="form-group">
                          <label for="masa_berlaku_sim">Masa berlaku SIM</label>
                          <input type="text" class="form-control" id="masa_berlaku_sim" name="masa_berlaku_sim" placeholder="Masa berlaku SIM">
                        </div>
                        <div class="form-group">
                          <label for="golongan_darah">Golongan darah</label>
                          <input type="text" class="form-control" id="golongan_darah" name="golongan_darah" placeholder="Golongan darah">
                        </div>
                        <div class="form-group">
                          <label for="ukuran_baju">Ukuran baju</label>
                          <input type="text" class="form-control" id="ukuran_baju" name="ukuran_baju" placeholder="Ukuran baju">
                        </div>
                        <div class="form-group">
                          <label for="berat_badan">Berat badan</label>
                          <input type="number" class="form-control" id="berat_badan" name="berat_badan" placeholder="Berat badan">
                        </div>
                        <div class="form-group">
                          <label for="ukuran_sepatu">Ukuran sepatu</label>
                          <input type="number" class="form-control" id="ukuran_sepatu" name="ukuran_sepatu" placeholder="Ukuran sepatu">
                        </div>
                        <div class="form-group">
                          <label for="tinggi_badan">Tinggi badan</label>
                          <input type="number" class="form-control" id="tinggi_badan" name="tinggi_badan" placeholder="Tinggi badan">
                        </div>
                        <div class="form-group">
                          <label for="email">Email</label>
                          <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                        </div>
                        <a class="btn btn-primary" onclick="stepper.next()">Next</a>
                      </div>




                      <div id="keluarga-part" class="content" role="tabpanel" aria-labelledby="keluarga-part-trigger">
                        <label>Data Keluarga</label>
                        <div class="form-group d-flex">
                          <h5 class="mt-1 w-25">Ayah</h5>
                          <input type="text" class="form-control mr-1 w-50" id="nama_ayah" name="nama_ayah" placeholder="Nama lengkap">
                          <select class="form-control w-25 mr-1" id="jenis_kelamin_keluarga1" name="jenis_kelamin_ayah" aria-label="Default select example">
                            <option selected>Jenis Kelamin</option>
                            <option value="laki">Laki-laki</option>
                            <option value="perempuan">Perempuan</option>
                          </select>
                          <input type="date" class="form-control mr-1 w-25" id="tanggal_lahir_ayah" name="tanggal_lahir_ayah" placeholder="Tanggal Lahir">
                          <input type="text" class="form-control mr-1 w-25" id="pendidikan_ayah" name="pendidikan_ayah" placeholder="Pendidikan">
                          <input type="text" class="form-control mr-1 w-25" id="pekerjaan_ayah" name="pekerjaan_ayah" placeholder="Pekerjaan">
                        </div>
                        <div class="form-group d-flex">
                          <h5 class="mt-1 w-25">Ibu</h5>
                          <input type="text" class="form-control mr-1 w-50" id="nama_ibu" name="nama_ibu" placeholder="Nama lengkap">
                          <select class="form-control w-25 mr-1" id="jenis_kelamin_ibu" name="jenis_kelamin_ibu" aria-label="Default select example">
                            <option selected>Jenis Kelamin</option>
                            <option value="laki">Laki-laki</option>
                            <option value="perempuan">Perempuan</option>
                          </select>
                          <input type="date" class="form-control mr-1 w-25" id="tanggal_lahir_ibu" name="tanggal_lahir_ibu" placeholder="Tanggal Lahir">
                          <input type="text" class="form-control mr-1 w-25" id="pendidikan_ibu" name="pendidikan_ibu" placeholder="Pendidikan">
                          <input type="text" class="form-control mr-1 w-25" id="pekerjaan_ibu" name="pekerjaan_ibu" placeholder="Pekerjaan">
                        </div>
                        <div class="form-group d-flex">
                          <h5 class="mt-1 w-25">Saudara 1</h5>
                          <input type="text" class="form-control mr-1 w-50" id="nama_saudara1" name="nama_saudara1" placeholder="Nama lengkap">
                          <select class="form-control w-25 mr-1" id="jenis_kelamin_saudara1" name="jenis_kelamin_saudara1" aria-label="Default select example">
                            <option selected>Jenis Kelamin</option>
                            <option value="laki">Laki-laki</option>
                            <option value="perempuan">Perempuan</option>
                          </select>
                          <input type="date" class="form-control mr-1 w-25" id="tanggal_lahir_saudara1" name="tanggal_lahir_saudara1" placeholder="Tanggal Lahir">
                          <input type="text" class="form-control mr-1 w-25" id="pendidikan_saudara1" name="pendidikan_saudara1" placeholder="Pendidikan">
                          <input type="text" class="form-control mr-1 w-25" id="pekerjaan_keluarga1" name="pekerjaan_keluarga1" placeholder="Pekerjaan">
                        </div>
                        <div class="form-group d-flex">
                          <h5 class="mt-1 w-25">Saudara 2</h5>
                          <input type="text" class="form-control mr-1 w-50" id="nama_saudara2" name="nama_saudara2" placeholder="Nama lengkap">
                          <select class="form-control w-25 mr-1" id="jenis_kelamin_saudara2" name="jenis_kelamin_saudara2" aria-label="Default select example">
                            <option selected>Jenis Kelamin</option>
                            <option value="laki">Laki-laki</option>
                            <option value="perempuan">Perempuan</option>
                          </select>
                          <input type="date" class="form-control mr-1 w-25" id="tanggal_lahir_saudara2" name="tanggal_lahir_saudara2" placeholder="Tanggal Lahir">
                          <input type="text" class="form-control mr-1 w-25" id="pendidikan_saudara2" name="pendidikan_saudara2" placeholder="Pendidikan">
                          <input type="text" class="form-control mr-1 w-25" id="pekerjaan_saudara2" name="pekerjaan_saudara2" placeholder="Pekerjaan">
                        </div>
                        <div class="form-group d-flex">
                          <h5 class="mt-1 w-25">Saudara 3</h5>
                          <input type="text" class="form-control mr-1 w-50" id="nama_saudara3" name="nama_saudara3" placeholder="Nama lengkap">
                          <select class="form-control w-25 mr-1" id="jenis_kelamin_saudara3" name="jenis_kelamin_saudara3" aria-label="Default select example">
                            <option selected>Jenis Kelamin</option>
                            <option value="laki">Laki-laki</option>
                            <option value="perempuan">Perempuan</option>
                          </select>
                          <input type="date" class="form-control mr-1 w-25" id="tanggal_lahir_saudara3" name="tanggal_lahir_saudara3" placeholder="Tanggal Lahir">
                          <input type="text" class="form-control mr-1 w-25" id="pendidikan_saudara3" name="pendidikan_saudara3" placeholder="Pendidikan">
                          <input type="text" class="form-control mr-1 w-25" id="pekerjaan_saudara3" name="pekerjaan_saudara3" placeholder="Pekerjaan">
                        </div>
                        <div class="form-group d-flex">
                          <h5 class="mt-1 w-25">Saudara 4</h5>
                          <input type="text" class="form-control mr-1 w-50" id="nama_saudara4" name="nama_saudara4" placeholder="Nama lengkap">
                          <select class="form-control w-25 mr-1" id="jenis_kelamin_saudara4" name="jenis_kelamin_saudara4" aria-label="Default select example">
                            <option selected>Jenis Kelamin</option>
                            <option value="laki">Laki-laki</option>
                            <option value="perempuan">Perempuan</option>
                          </select>
                          <input type="date" class="form-control mr-1 w-25" id="tanggal_lahir_saudara4" name="tanggal_lahir_saudara4" placeholder="Tanggal Lahir">
                          <input type="text" class="form-control mr-1 w-25" id="pendidikan_saudara4" name="pendidikan_saudara4" placeholder="Pendidikan">
                          <input type="text" class="form-control mr-1 w-25" id="pekerjaan_saudara4" name="pekerjaan_saudara4" placeholder="Pekerjaan">
                        </div>

                        <hr>

                        <label>Isi bila sudah menikah</label>
                        <div class="form-group d-flex">
                          <h5 class="mt-1 w-25">Suami</h5>
                          <input type="text" class="form-control mr-1 w-50" id="nama_suami" name="nama_suami" placeholder="Nama lengkap">
                          <select class="form-control w-25 mr-1" id="jenis_kelamin_suami" name="jenis_kelamin_suami" aria-label="Default select example">
                            <option selected>Jenis Kelamin</option>
                            <option value="laki">Laki-laki</option>
                            <option value="perempuan">Perempuan</option>
                          </select>
                          <input type="date" class="form-control mr-1 w-25" id="tanggal_lahir_suami" name="tanggal_lahir_suami" placeholder="Tanggal Lahir">
                          <input type="text" class="form-control mr-1 w-25" id="pendidikan_suami" name="pendidikan_suami" placeholder="Pendidikan">
                          <input type="text" class="form-control mr-1 w-25" id="pekerjaan_suami" name="pekerjaan_suami" placeholder="Pekerjaan">
                        </div>
                        <div class="form-group d-flex">
                          <h5 class="mt-1 w-25">Istri</h5>
                          <input type="text" class="form-control mr-1 w-50" id="nama_istri" name="nama_istri" placeholder="Nama lengkap">
                          <select class="form-control w-25 mr-1" id="jenis_kelamin_istri" name="jenis_kelamin_istri" aria-label="Default select example">
                            <option selected>Jenis Kelamin</option>
                            <option value="laki">Laki-laki</option>
                            <option value="perempuan">Perempuan</option>
                          </select>
                          <input type="date" class="form-control mr-1 w-25" id="tanggal_lahir_istri" name="tanggal_lahir_istri" placeholder="Tanggal Lahir">
                          <input type="text" class="form-control mr-1 w-25" id="pendidikan_istri" name="pendidikan_istri" placeholder="Pendidikan">
                          <input type="text" class="form-control mr-1 w-25" id="pekerjaan_istri" name="pekerjaan_istri" placeholder="Pekerjaan">
                        </div>
                        <div class="form-group d-flex">
                          <h5 class="mt-1 w-25">Anak 1</h5>
                          <input type="text" class="form-control mr-1 w-50" id="nama_anak1" name="nama_anak1" placeholder="Nama lengkap">
                          <select class="form-control w-25 mr-1" id="jenis_kelamin_anak1" name="jenis_kelamin_anak1" aria-label="Default select example">
                            <option selected>Jenis Kelamin</option>
                            <option value="laki">Laki-laki</option>
                            <option value="perempuan">Perempuan</option>
                          </select>
                          <input type="date" class="form-control mr-1 w-25" id="tanggal_lahir_anak1" name="tanggal_lahir_anak1" placeholder="Tanggal Lahir">
                          <input type="text" class="form-control mr-1 w-25" id="pendidikan_anak1" name="pendidikan_anak1" placeholder="Pendidikan">
                          <input type="text" class="form-control mr-1 w-25" id="pekerjaan_anak1" name="pekerjaan_anak1" placeholder="Pekerjaan">
                        </div>
                        <div class="form-group d-flex">
                          <h5 class="mt-1 w-25">Anak 2</h5>
                          <input type="text" class="form-control mr-1 w-50" id="nama_anak2" name="nama_anak2" placeholder="Nama lengkap">
                          <select class="form-control w-25 mr-1" id="jenis_kelamin_anak2" name="jenis_kelamin_anak2" aria-label="Default select example">
                            <option selected>Jenis Kelamin</option>
                            <option value="laki">Laki-laki</option>
                            <option value="perempuan">Perempuan</option>
                          </select>
                          <input type="date" class="form-control mr-1 w-25" id="tanggal_lahir_anak2" name="tanggal_lahir_anak2" placeholder="Tanggal Lahir">
                          <input type="text" class="form-control mr-1 w-25" id="pendidikan_anak2" name="pendidikan_anak2" placeholder="Pendidikan">
                          <input type="text" class="form-control mr-1 w-25" id="pekerjaan_anak2" name="pekerjaan_anak2" placeholder="Pekerjaan">
                        </div>
                        <div class="form-group d-flex">
                          <h5 class="mt-1 w-25">Anak 3</h5>
                          <input type="text" class="form-control mr-1 w-50" id="nama_anak3" name="nama_anak3" placeholder="Nama lengkap">
                          <select class="form-control w-25 mr-1" id="jenis_kelamin_anak3" name="jenis_kelamin_anak3" aria-label="Default select example">
                            <option selected>Jenis Kelamin</option>
                            <option value="laki">Laki-laki</option>
                            <option value="perempuan">Perempuan</option>
                          </select>
                          <input type="date" class="form-control mr-1 w-25" id="tanggal_lahir_anak3" name="tanggal_lahir_anak3" placeholder="Tanggal Lahir">
                          <input type="text" class="form-control mr-1 w-25" id="pendidikan_anak3" name="pendidikan_anak3" placeholder="Pendidikan">
                          <input type="text" class="form-control mr-1 w-25" id="pekerjaan_anak3" name="pekerjaan_anak3" placeholder="Pekerjaan">
                        </div>
                        <a class="btn btn-primary" onclick="stepper.previous()">Previous</a>
                        <a class="btn btn-primary" onclick="stepper.next()">Next</a>

                      </div>

                      <div id="pendidikan-part" class="content" role="tabpanel" aria-labelledby="pendidikan-part-trigger">
                        <div class="form-group">
                          <label for="sekolah">Nama sekolah</label>
                          <input type="text" class="form-control" id="sekolah" name="sekolah" placeholder="Nama sekolah">
                        </div>
                        <label for="dari_tahun_formal">Tahun</label>
                        <div class="form-group d-flex">
                          <input type="number" class="form-control mr-2" id="dari_tahun_formal" name="dari_tahun_formal" placeholder="Dari tahun">
                          <input type="number" class="form-control" id="sampai_tahun_formal" name="sampai_tahun_formal" placeholder="Sampai tahun">
                        </div>
                        <div class="form-group">
                          <label for="jurusan">Jurusan</label>
                          <input type="text" class="form-control" id="jurusan" name="jurusan" placeholder="Jurusan">
                        </div>
                        <label for="tempat_formal">Tempat</label>
                        <div class="form-group d-flex">
                          <input type="text" class="form-control mr-2" id="tempat_formal" name="tempat_formal" placeholder="Tempat">
                          <select class="form-control" id="ijazah_formal" name="ijazah_formal" aria-label="Default select example">
                            <option selected>Ijazah</option>
                            <option value="ada">Ada</option>
                            <option value="tidak">Tidak</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="keterangan_formal">Keterangan</label>
                          <input type="text" class="form-control" id="keterangan_formal" name="keterangan_formal" placeholder="Keterangan">
                        </div>

                        <hr>

                        <div class="form-group">
                          <label for="sekolah2">Nama sekolah</label>
                          <input type="text" class="form-control" id="sekolah2" name="sekolah2" placeholder="Nama sekolah">
                        </div>
                        <label for="dari_tahun_formal">Tahun</label>
                        <div class="form-group d-flex">
                          <input type="number" class="form-control mr-2" id="dari_tahun_formal2" name="dari_tahun_formal2" placeholder="Dari tahun">
                          <input type="number" class="form-control" id="sampai_tahun_formal2" name="sampai_tahun_formal2" placeholder="Sampai tahun">
                        </div>
                        <div class="form-group">
                          <label for="jurusan2">Jurusan</label>
                          <input type="text" class="form-control" id="jurusan2" name="jurusan2" placeholder="Jurusan">
                        </div>
                        <label for="tempat_formal2">Tempat</label>
                        <div class="form-group d-flex">
                          <input type="text" class="form-control mr-2" id="tempat_formal2" name="tempat_formal2" placeholder="Tempat">
                          <select class="form-control" id="ijazah_formal2" name="ijazah_formal2" aria-label="Default select example">
                            <option selected>Ijazah</option>
                            <option value="ada">Ada</option>
                            <option value="tidak">Tidak</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="keterangan_formal2">Keterangan</label>
                          <input type="text" class="form-control" id="keterangan_formal2" name="keterangan_formal2" placeholder="Keterangan">
                        </div>

                        <hr>

                        <div class="form-group">
                          <label for="sekolah3">Nama sekolah</label>
                          <input type="text" class="form-control" id="sekolah3" name="sekolah3" placeholder="Nama sekolah">
                        </div>
                        <label for="dari_tahun_formal3">Tahun</label>
                        <div class="form-group d-flex">
                          <input type="number" class="form-control mr-2" id="dari_tahun_formal3" name="dari_tahun_formal3" placeholder="Dari tahun">
                          <input type="number" class="form-control" id="sampai_tahun_formal3" name="sampai_tahun_formal3" placeholder="Sampai tahun">
                        </div>
                        <div class="form-group">
                          <label for="jurusan3">Jurusan</label>
                          <input type="text" class="form-control" id="jurusan3" name="jurusan3" placeholder="Jurusan">
                        </div>
                        <label for="tempat_formal3">Tempat</label>
                        <div class="form-group d-flex">
                          <input type="text" class="form-control mr-2" id="tempat_formal3" name="tempat_formal3" placeholder="Tempat">
                          <select class="form-control" id="ijazah_formal3" name="ijazah_formal3" aria-label="Default select example">
                            <option selected>Ijazah</option>
                            <option value="ada">Ada</option>
                            <option value="tidak">Tidak</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="keterangan_formal3">Keterangan</label>
                          <input type="text" class="form-control" id="keterangan_formal3" name="keterangan_formal3" placeholder="Keterangan">
                        </div>

                        <hr>

                        <div class="form-group">
                          <label for="sekolah4">Nama sekolah</label>
                          <input type="text" class="form-control" id="sekolah4" name="sekolah4" placeholder="Nama sekolah">
                        </div>
                        <label for="dari_tahun_formal4">Tahun</label>
                        <div class="form-group d-flex">
                          <input type="number" class="form-control mr-2" id="dari_tahun_formal4" name="dari_tahun_formal4" placeholder="Dari tahun">
                          <input type="number" class="form-control" id="sampai_tahun_formal" name="sampai_tahun_formal4" placeholder="Sampai tahun4">
                        </div>
                        <div class="form-group">
                          <label for="jurusan4">Jurusan</label>
                          <input type="text" class="form-control" id="jurusan4" name="jurusan4" placeholder="Jurusan">
                        </div>
                        <label for="tempat_formal4">Tempat</label>
                        <div class="form-group d-flex">
                          <input type="text" class="form-control mr-2" id="tempat_formal4" name="tempat_formal4" placeholder="Tempat">
                          <select class="form-control" id="ijazah_formal4" name="ijazah_formal4" aria-label="Default select example">
                            <option selected>Ijazah</option>
                            <option value="ada">Ada</option>
                            <option value="tidak">Tidak</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="keterangan_formal4">Keterangan</label>
                          <input type="text" class="form-control" id="keterangan_formal4" name="keterangan_formal4" placeholder="Keterangan">
                        </div>

                        <a class="btn btn-primary" onclick="stepper.previous()">Previous</a>
                        <a class="btn btn-primary" onclick="stepper.next()">Next</a>
                      </div>

                      <div id="nonformal-part" class="content" role="tabpanel" aria-labelledby="nonformal-part-trigger">
                        <label for="pelatihan">Pelatihan</label>
                        <div class="form-group d-flex">
                          <input type="text" class="form-control mr-2" id="pelatihan" name="pelatihan" placeholder="Pelatihan">
                          <input type="number" class="form-control" id="tahun_pelatihan" name="tahun_pelatihan" placeholder="tahun">
                        </div>
                        <label for="penyelenggara_pelatihan">Penyelenggara</label>
                        <div class="form-group d-flex">
                          <input type="text" class="form-control mr-2" id="penyelenggara_pelatihan" name="penyelenggara_pelatihan" placeholder="Penyelenggara">
                          <input type="text" class="form-control" id="tempat_pelatihan" name="tempat_pelatihan" placeholder="Tempat">
                        </div>
                        <div class="form-group">
                          <label for="sertifikat_pelatihan">Sertifikat</label>
                          <select class="form-control" id="sertifikat_pelatihan" name="sertifikat_pelatihan" aria-label="Default select example">
                            <option selected>Select</option>
                            <option value="ada">Ada</option>
                            <option value="tidak">Tidak</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="keterangan_pelatihan">Keterangan</label>
                          <input type="text" class="form-control" id="keterangan_pelatihan" name="keterangan_pelatihan" placeholder="Keterangan">
                        </div>

                        <hr>

                        <label for="pelatihan2">Pelatihan</label>
                        <div class="form-group d-flex">
                          <input type="text" class="form-control mr-2" id="pelatihan2" name="pelatihan2" placeholder="Pelatihan">
                          <input type="number" class="form-control" id="tahun_pelatihan2" name="tahun_pelatihan2" placeholder="tahun">
                        </div>
                        <label for="penyelenggara_pelatihan2">Penyelenggara</label>
                        <div class="form-group d-flex">
                          <input type="text" class="form-control mr-2" id="penyelenggara_pelatihan2" name="penyelenggara_pelatihan2" placeholder="Penyelenggara">
                          <input type="text" class="form-control" id="tempat_pelatihan2" name="tempat_pelatihan2" placeholder="Tempat">
                        </div>
                        <div class="form-group">
                          <label for="sertifikat_pelatihan2">Sertifikat</label>
                          <select class="form-control" id="sertifikat_pelatihan2" name="sertifikat_pelatihan2" aria-label="Default select example">
                            <option selected>Select</option>
                            <option value="ada">Ada</option>
                            <option value="tidak">Tidak</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="keterangan_pelatihan2">Keterangan</label>
                          <input type="text" class="form-control" id="keterangan_pelatihan2" name="keterangan_pelatihan2" placeholder="Keterangan">
                        </div>

                        <hr>

                        <label for="pelatihan3">Pelatihan</label>
                        <div class="form-group d-flex">
                          <input type="text" class="form-control mr-2" id="pelatihan3" name="pelatihan3" placeholder="Pelatihan">
                          <input type="number" class="form-control" id="tahun_pelatihan3" name="tahun_pelatihan3" placeholder="tahun">
                        </div>
                        <label for="penyelenggara_pelatihan3">Penyelenggara</label>
                        <div class="form-group d-flex">
                          <input type="text" class="form-control mr-2" id="penyelenggara_pelatihan3" name="penyelenggara_pelatihan3" placeholder="Penyelenggara">
                          <input type="text" class="form-control" id="tempat_pelatihan3" name="tempat_pelatihan3" placeholder="Tempat">
                        </div>
                        <div class="form-group">
                          <label for="sertifikat_pelatihan3">Sertifikat</label>
                          <select class="form-control" id="sertifikat_pelatihan3" name="sertifikat_pelatihan3" aria-label="Default select example">
                            <option selected>Select</option>
                            <option value="ada">Ada</option>
                            <option value="tidak">Tidak</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="keterangan_pelatihan3">Keterangan</label>
                          <input type="text" class="form-control" id="keterangan_pelatihan3" name="keterangan_pelatihan3" placeholder="Keterangan">
                        </div>

                        <a class="btn btn-primary" onclick="stepper.previous()">Previous</a>
                        <a class="btn btn-primary" onclick="stepper.next()">Next</a>
                      </div>

                      <div id="bahasa-part" class="content" role="tabpanel" aria-labelledby="bahasa-part-trigger">
                        <div class="form-group">
                          <label for="bahasa">Bahasa</label>
                          <input type="text" class="form-control" id="bahasa" name="bahasa" placeholder="Bahasa">
                        </div>
                        <div class="form-group">
                          <label for="berbicara">Berbicara</label>
                          <input type="text" class="form-control" id="berbicara" name="berbicara" placeholder="Berbicara">
                        </div>
                        <div class="form-group">
                          <label for="membaca">Membaca</label>
                          <input type="text" class="form-control" id="membaca" name="membaca" placeholder="Membaca">
                        </div>
                        <div class="form-group">
                          <label for="menulis">Menulis</label>
                          <input type="text" class="form-control" id="menulis" name="menulis" placeholder="Menulis">
                        </div>

                        <hr>

                        <div class="form-group">
                          <label for="bahasa2">Bahasa</label>
                          <input type="text" class="form-control" id="bahasa2" name="bahasa2" placeholder="Bahasa">
                        </div>
                        <div class="form-group">
                          <label for="berbicara2">Berbicara</label>
                          <input type="text" class="form-control" id="berbicara2" name="berbicara2" placeholder="Berbicara">
                        </div>
                        <div class="form-group">
                          <label for="membaca2">Membaca</label>
                          <input type="text" class="form-control" id="membaca2" name="membaca2" placeholder="Membaca">
                        </div>
                        <div class="form-group">
                          <label for="menulis2">Menulis</label>
                          <input type="text" class="form-control" id="menulis2" name="menulis2" placeholder="Menulis">
                        </div>

                        <hr>

                        <div class="form-group">
                          <label for="bahasa3">Bahasa</label>
                          <input type="text" class="form-control" id="bahasa3" name="bahasa3" placeholder="Bahasa">
                        </div>
                        <div class="form-group">
                          <label for="berbicara3">Berbicara</label>
                          <input type="text" class="form-control" id="berbicara3" name="berbicara3" placeholder="Berbicara">
                        </div>
                        <div class="form-group">
                          <label for="membaca3">Membaca</label>
                          <input type="text" class="form-control" id="membaca3" name="membaca3" placeholder="Membaca">
                        </div>
                        <div class="form-group">
                          <label for="menulis3">Menulis</label>
                          <input type="text" class="form-control" id="menulis3" name="menulis3" placeholder="Menulis">
                        </div>

                        <a class="btn btn-primary" onclick="stepper.previous()">Previous</a>
                        <a class="btn btn-primary" onclick="stepper.next()">Next</a>
                      </div>

                      <div id="prestasi-part" class="content" role="tabpanel" aria-labelledby="prestasi-part-trigger">
                        <div class="form-group">
                          <label for="kejuaraan">Kejuaraan</label>
                          <input type="text" class="form-control" id="kejuaraan" name="kejuaraan" placeholder="Kejuaraan">
                        </div>
                        <div class="form-group">
                          <label for="bidang">Bidang</label>
                          <input type="text" class="form-control" id="bidang" name="bidang" placeholder="Bidang">
                        </div>
                        <div class="form-group">
                          <label for="penyelenggara_kejuaraan">Penyelenggara</label>
                          <input type="text" class="form-control" id="penyelenggara_kejuaraan" name="penyelenggara_kejuaraan" placeholder="Penyelenggara">
                        </div>
                        <div class="form-group">
                          <label for="rengking">Rengking</label>
                          <input type="text" class="form-control" id="rengking" name="rengking" placeholder="Rengking">
                        </div>
                        <div class="form-group">
                          <label for="keterangan_kejuaraan">Keterangan</label>
                          <input type="text" class="form-control" id="keterangan_kejuaraan" name="keterangan_kejuaraan" placeholder="Keterangan">
                        </div>

                        <hr>

                        <div class="form-group">
                          <label for="kejuaraan2">Kejuaraan</label>
                          <input type="text" class="form-control" id="kejuaraan2" name="kejuaraan2" placeholder="Kejuaraan">
                        </div>
                        <div class="form-group">
                          <label for="bidang2">Bidang</label>
                          <input type="text" class="form-control" id="bidang2" name="bidang2" placeholder="Bidang">
                        </div>
                        <div class="form-group">
                          <label for="penyelenggara_kejuaraan2">Penyelenggara</label>
                          <input type="text" class="form-control" id="penyelenggara_kejuaraan2" name="penyelenggara_kejuaraan2" placeholder="Penyelenggara">
                        </div>
                        <div class="form-group">
                          <label for="rengking2">Rengking</label>
                          <input type="text" class="form-control" id="rengking2" name="rengking2" placeholder="Rengking">
                        </div>
                        <div class="form-group">
                          <label for="keterangan_kejuaraan2">Keterangan</label>
                          <input type="text" class="form-control" id="keterangan_kejuaraan2" name="keterangan_kejuaraan2" placeholder="Keterangan">
                        </div>

                        <hr>

                        <div class="form-group">
                          <label for="kejuaraan3">Kejuaraan</label>
                          <input type="text" class="form-control" id="kejuaraan3" name="kejuaraan3" placeholder="Kejuaraan">
                        </div>
                        <div class="form-group">
                          <label for="bidang3">Bidang</label>
                          <input type="text" class="form-control" id="bidang3" name="bidang3" placeholder="Bidang">
                        </div>
                        <div class="form-group">
                          <label for="penyelenggara_kejuaraan3">Penyelenggara</label>
                          <input type="text" class="form-control" id="penyelenggara_kejuaraan3" name="penyelenggara_kejuaraan3" placeholder="Penyelenggara">
                        </div>
                        <div class="form-group">
                          <label for="rengking3">Rengking</label>
                          <input type="text" class="form-control" id="rengking3" name="rengking3" placeholder="Rengking">
                        </div>
                        <div class="form-group">
                          <label for="keterangan_kejuaraan3">Keterangan</label>
                          <input type="text" class="form-control" id="keterangan_kejuaraan3" name="keterangan_kejuaraan3" placeholder="Keterangan">
                        </div>

                        <a class="btn btn-primary" onclick="stepper.previous()">Previous</a>
                        <a class="btn btn-primary" onclick="stepper.next()">Next</a>
                      </div>

                      <div id="keahlian-part" class="content" role="tabpanel" aria-labelledby="keahlian-part-trigger">
                        <div class="form-group">
                          <label for="nama_keahlian">Nama keahlian</label>
                          <input type="text" class="form-control" id="nama_keahlian" name="nama_keahlian" placeholder="Nama keahlian">
                        </div>
                        <div class="form-group">
                          <label for="tingkat_keahlian">Tingkat keahlian</label>
                          <select class="form-control" id="tingkat_keahlian" name="tingkat_keahlian" aria-label="Default select example">
                            <option selected>Select</option>
                            <option value="PEMULA">Pemula</option>
                            <option value="INTERMEDIATE">Intermediate</option>
                            <option value="MAHIR">Mahir</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="sertifikat_keahlian">sertifikat</label>
                          <input type="text" class="form-control" id="sertifikat_keahlian" name="sertifikat_keahlian" placeholder="sertifikat">
                        </div>

                        <hr>

                        <div class="form-group">
                          <label for="nama_keahlian2">Nama keahlian</label>
                          <input type="text" class="form-control" id="nama_keahlian2" name="nama_keahlian2" placeholder="Nama keahlian">
                        </div>
                        <div class="form-group">
                          <label for="tingkat_keahlian2">Tingkat keahlian</label>
                          <select class="form-control" id="tingkat_keahlian2" name="tingkat_keahlian2" aria-label="Default select example">
                            <option selected>Select</option>
                            <option value="PEMULA">Pemula</option>
                            <option value="INTERMEDIATE">Intermediate</option>
                            <option value="MAHIR">Mahir</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="sertifikat_keahlian2">sertifikat</label>
                          <input type="text" class="form-control" id="sertifikat_keahlian2" name="sertifikat_keahlian2" placeholder="sertifikat">
                        </div>

                        <hr>

                        <div class="form-group">
                          <label for="nama_keahlian3">Nama keahlian</label>
                          <input type="text" class="form-control" id="nama_keahlian3" name="nama_keahlian3" placeholder="Nama keahlian">
                        </div>
                        <div class="form-group">
                          <label for="tingkat_keahlian3">Tingkat keahlian</label>
                          <select class="form-control" id="tingkat_keahlian3" name="tingkat_keahlian3" aria-label="Default select example">
                            <option selected>Select</option>
                            <option value="PEMULA">Pemula</option>
                            <option value="INTERMEDIATE">Intermediate</option>
                            <option value="MAHIR">Mahir</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="sertifikat_keahlian3">sertifikat</label>
                          <input type="text" class="form-control" id="sertifikat_keahlian3" name="sertifikat_keahlian3" placeholder="sertifikat">
                        </div>

                        <a class="btn btn-primary" onclick="stepper.previous()">Previous</a>
                        <a class="btn btn-primary" onclick="stepper.next()">Next</a>
                      </div>

                      <div id="hobby-part" class="content" role="tabpanel" aria-labelledby="hobby-part-trigger">
                        <div class="form-group">
                          <label for="hoby">Hoby</label>
                          <input type="text" class="form-control" id="hoby" name="hoby" placeholder="Hoby">
                        </div>

                        <hr>
                        <h6>Organisasi jika ada</h6>
                        <div class="form-group">
                          <label for="nama_organisasi">Nama organisasi</label>
                          <input type="text" class="form-control" id="nama_organisasi" name="nama_organisasi" placeholder="Nama organisasi">
                        </div>
                        <div class="form-group">
                          <label for="jenis_organisasi">Jenis organisasi</label>
                          <input type="text" class="form-control" id="jenis_organisasi" name="jenis_organisasi" placeholder="Jenis organisasi">
                        </div>
                        <div class="form-group">
                          <label for="tahun_kegiatan">Tahun</label>
                          <input type="number" class="form-control" id="Tahun_kegiatan" name="tahun_kegiatan" placeholder="tahun">
                        </div>
                        <div class="form-group">
                          <label for="jabatan_organisasi">Jabatan</label>
                          <input type="text" class="form-control" id="jabatan_organisasi" name="jabatan_organisasi" placeholder="Jabatan">
                        </div>
                        <a class="btn btn-primary" onclick="stepper.previous()">Previous</a>
                        <a class="btn btn-primary" onclick="stepper.next()">Next</a>
                      </div>

                      <div id="pengalaman-part" class="content" role="tabpanel" aria-labelledby="pengalaman-part-trigger">
                        <div class="form-group">
                          <label for="nama_perusahaan">Nama perusahaan</label>
                          <input type="text" class="form-control" id="nama_perusahaan" name="nama_perusahaan" placeholder="Nama perusahaan">
                        </div>
                        <label for="dari_tahun_pengalaman">Dari tahun</label>
                        <div class="form-group d-flex">
                          <input type="text" class="form-control mr-2" id="dari_tahun_pengalaman" name="dari_tahun_pengalaman" placeholder="Dari tahun">
                          <input type="text" class="form-control" id="sampai_tahun_pengalaman" name="sampai_tahun_pengalaman" placeholder="Sampai tahun">
                        </div>
                        <div class="form-group">
                          <label for="alasan_berhenti">Alasan berhenti</label>
                          <input type="text" class="form-control" id="alasan_berhenti" name="alasan_berhenti" placeholder="Alasan berhenti">
                        </div>
                        <div class="form-group">
                          <label for="gaji">Gaji</label>
                          <input type="text" class="form-control" id="gaji" name="gaji" placeholder="Gaji">
                        </div>

                        <hr>

                        <div class="form-group">
                          <label for="nama_perusahaan2">Nama perusahaan</label>
                          <input type="text" class="form-control" id="nama_perusahaan2" name="nama_perusahaan2" placeholder="Nama perusahaan">
                        </div>
                        <label for="dari_tahun_pengalaman2">Dari tahun</label>
                        <div class="form-group d-flex">
                          <input type="text" class="form-control mr-2" id="dari_tahun_pengalaman2" name="dari_tahun_pengalaman2" placeholder="Dari tahun">
                          <input type="text" class="form-control" id="sampai_tahun_pengalaman2" name="sampai_tahun_pengalaman2" placeholder="Sampai tahun">
                        </div>
                        <div class="form-group">
                          <label for="alasan_berhenti2">Alasan berhenti</label>
                          <input type="text" class="form-control" id="alasan_berhenti2" name="alasan_berhenti2" placeholder="Alasan berhenti">
                        </div>
                        <div class="form-group">
                          <label for="gaji2">Gaji</label>
                          <input type="text" class="form-control" id="gaji2" name="gaji2" placeholder="Gaji">
                        </div>

                        <hr>

                        <div class="form-group">
                          <label for="nama_perusahaan3">Nama perusahaan</label>
                          <input type="text" class="form-control" id="nama_perusahaan3" name="nama_perusahaan3" placeholder="Nama perusahaan">
                        </div>
                        <label for="dari_tahun_pengalaman3">Dari tahun</label>
                        <div class="form-group d-flex">
                          <input type="text" class="form-control mr-2" id="dari_tahun_pengalaman3" name="dari_tahun_pengalaman3" placeholder="Dari tahun">
                          <input type="text" class="form-control" id="sampai_tahun_pengalaman3" name="sampai_tahun_pengalaman3" placeholder="Sampai tahun">
                        </div>
                        <div class="form-group">
                          <label for="alasan_berhenti3">Alasan berhenti</label>
                          <input type="text" class="form-control" id="alasan_berhenti3" name="alasan_berhenti3" placeholder="Alasan berhenti">
                        </div>
                        <div class="form-group">
                          <label for="gaji3">Gaji</label>
                          <input type="text" class="form-control" id="gaji3" name="gaji3" placeholder="Gaji">
                        </div>

                        <hr>


                        <h6>Nama atasan</h6>
                        <label for="nama_atasan">Nama atasan</label>
                        <div class="form-group d-flex">
                          <input type="text" class="form-control mr-2" id="nama_atasan" name="nama_atasan" placeholder="Nama atasan">
                          <input type="text" class="form-control" id="jabatan_atasan" name="jabatan_atasan" placeholder="Jabatan atasan">
                        </div>
                        <label for="telpon_atasan">Telpon atasan</label>
                        <div class="form-group d-flex">
                          <input type="text" class="form-control mr-2" id="telpon_atasan" name="telpon_atasan" placeholder="Telpon atasan">
                          <input type="text" class="form-control" id="perusahaan_atasan" name="perusahaan_atasan" placeholder="Perusahaan atasan">
                        </div>

                        <hr>

                        <label for="nama_atasan2">Nama atasan</label>
                        <div class="form-group d-flex">
                          <input type="text" class="form-control mr-2" id="nama_atasan2" name="nama_atasan2" placeholder="Nama atasan">
                          <input type="text" class="form-control" id="jabatan_atasan2" name="jabatan_atasan2" placeholder="Jabatan atasan">
                        </div>
                        <label for="telpon_atasan2">Telpon atasan</label>
                        <div class="form-group d-flex">
                          <input type="text" class="form-control mr-2" id="telpon_atasan2" name="telpon_atasan2" placeholder="Telpon atasan">
                          <input type="text" class="form-control" id="perusahaan_atasan2" name="perusahaan_atasan2" placeholder="Perusahaan atasan">
                        </div>

                        <hr>

                        <label for="nama_atasan3">Nama atasan</label>
                        <div class="form-group d-flex">
                          <input type="text" class="form-control mr-2" id="nama_atasan3" name="nama_atasan3" placeholder="Nama atasan">
                          <input type="text" class="form-control" id="jabatan_atasan3" name="jabatan_atasan3" placeholder="Jabatan atasan">
                        </div>
                        <label for="telpon_atasan3">Telpon atasan</label>
                        <div class="form-group d-flex">
                          <input type="text" class="form-control mr-2" id="telpon_atasan3" name="telpon_atasan3" placeholder="Telpon atasan">
                          <input type="text" class="form-control" id="perusahaan_atasan3" name="perusahaan_atasan3" placeholder="Perusahaan atasan">
                        </div>

                        <hr>

                        <div class="form-group">
                          <label for="uraian_singkat_jabatan">Berilah uraian singkat</label>
                          <textarea class="form-control" placeholder="Berilah secara singkat uraian pekerjaan dari jabatan terakhir yang pernah anda jabat" id="uraian_singkat_jabatan" id="uraian_singkat_jabatan" style="height: 100px"></textarea>
                        </div>

                        <hr>

                        <div class="form-group">
                          <h6>Berapa jumlah karyawan yang pernah anda pimpin (bila ada)?</h6>
                          <label for="jabatan">Jabatan</label>
                          <input type="text" class="form-control" id="jabatan" name="jabatan" placeholder="Jabatan">
                        </div>
                        <div class="form-group">
                          <label for="perusahaan">Perusahaan</label>
                          <input type="text" class="form-control" id="perusahaan" name="perusahaan" placeholder="Perusahaan">
                        </div>
                        <div class="form-group">
                          <label for="jumlah_karyawan">Jumlah bawahan</label>
                          <input type="text" class="form-control" id="jumlah_karyawan" name="jumlah_karyawan" placeholder="Jumlah bawahan">
                        </div>

                        <hr>

                        <div class="form-group">
                          <h6>Berapa jumlah karyawan yang pernah anda pimpin (bila ada)?</h6>
                          <label for="jabatan2">Jabatan</label>
                          <input type="text" class="form-control" id="jabatan2" name="jabatan2" placeholder="Jabatan">
                        </div>
                        <div class="form-group">
                          <label for="perusahaan2">Perusahaan</label>
                          <input type="text" class="form-control" id="perusahaan2" name="perusahaan2" placeholder="Perusahaan">
                        </div>
                        <div class="form-group">
                          <label for="jumlah_karyawan2">Jumlah bawahan</label>
                          <input type="text" class="form-control" id="jumlah_karyawan2" name="jumlah_karyawan2" placeholder="Jumlah bawahan">
                        </div>

                        <hr>

                        <div class="form-group">
                          <h6>Berapa jumlah karyawan yang pernah anda pimpin (bila ada)?</h6>
                          <label for="jabatan3">Jabatan</label>
                          <input type="text" class="form-control" id="jabatan3" name="jabatan3" placeholder="Jabatan">
                        </div>
                        <div class="form-group">
                          <label for="perusahaan3">Perusahaan</label>
                          <input type="text" class="form-control" id="perusahaan3" name="perusahaan3" placeholder="Perusahaan">
                        </div>
                        <div class="form-group">
                          <label for="jumlah_karyawan3">Jumlah bawahan</label>
                          <input type="text" class="form-control" id="jumlah_karyawan3" name="jumlah_karyawan3" placeholder="Jumlah bawahan">
                        </div>

                        <hr>

                        <div class="form-group">
                          <label for="uraian_singkat_jabatan">Berilah uraian singkat</label>
                          <textarea class="form-control" placeholder="Puaskan anda pada kemajuan yang pernah anda capai pada pekerjaan terdahulu, Jelaskan?" id="uraian_singkat_jabatan" id="uraian_singkat_jabatan" style="height: 100px"></textarea>
                        </div>

                        <hr>

                        <div class="form-group">
                          <label for="gaji_pokok">Gaji pokok</label>
                          <input type="text" class="form-control" id="gaji_pokok" name="gaji_pokok" placeholder="Gaji pokok">
                        </div>
                        <div class="form-group">
                          <label for="lembur">Lembur</label>
                          <input type="text" class="form-control" id="lembur" name="lembur" placeholder="Lembur">
                        </div>
                        <div class="form-group">
                          <label for="makan">Makan</label>
                          <input type="text" class="form-control" id="makan" name="makan" placeholder="Makan">
                        </div>
                        <div class="form-group">
                          <label for="bonus">Bonus</label>
                          <input type="text" class="form-control" id="bonus" name="bonus" placeholder="Bonus">
                        </div>
                        <div class="form-group">
                          <label for="kesehatan">Kesehatan</label>
                          <input type="text" class="form-control" id="kesehatan" name="kesehatan" placeholder="Kesehatan">
                        </div>
                        <div class="form-group">
                          <label for="lain_lain">Lain lain</label>
                          <input type="text" class="form-control" id="lain_lain" name="lain_lain" placeholder="Lain lain">
                        </div>

                        <hr>

                        <div class="form-group">
                          <h6>Refrensi</h6>
                          <label for="nama_refrensi">Nama</label>
                          <input type="text" class="form-control" id="nama_refrensi" name="nama_refrensi" placeholder="Jabatan">
                        </div>
                        <div class="form-group">
                          <label for="alamat_refrensi">Alamat</label>
                          <input type="text" class="form-control" id="alamat_refrensi" name="alamat_refrensi" placeholder="Jabatan">
                        </div>
                        <div class="form-group">
                          <label for="pekerjaan_refrensi">Pekerjaan</label>
                          <input type="text" class="form-control" id="pekerjaan_refrensi" name="pekerjaan_refrensi" placeholder="Jabatan">
                        </div>
                        <div class="form-group">
                          <label for="telpon_refrensi">Telpon</label>
                          <input type="text" class="form-control" id="telpon_refrensi" name="telpon_refrensi" placeholder="Jabatan">
                        </div>
                        <div class="form-group">
                          <label for="tahun_kenal">Tahun kenal</label>
                          <input type="text" class="form-control" id="tahun_kenal" name="tahun_kenal" placeholder="Jabatan">
                        </div>

                        <hr>

                        <div class="form-group">
                          <label for="nama_refrensi2">Nama</label>
                          <input type="text" class="form-control" id="nama_refrensi2" name="nama_refrensi2" placeholder="Jabatan">
                        </div>
                        <div class="form-group">
                          <label for="alamat_refrensi2">Alamat</label>
                          <input type="text" class="form-control" id="alamat_refrensi2" name="alamat_refrensi2" placeholder="Jabatan">
                        </div>
                        <div class="form-group">
                          <label for="pekerjaan_refrensi2">Pekerjaan</label>
                          <input type="text" class="form-control" id="pekerjaan_refrensi2" name="pekerjaan_refrensi2" placeholder="Jabatan">
                        </div>
                        <div class="form-group">
                          <label for="telpon_refrensi2">Telpon</label>
                          <input type="text" class="form-control" id="telpon_refrensi2" name="telpon_refrensi2" placeholder="Jabatan">
                        </div>
                        <div class="form-group">
                          <label for="tahun_kenal2">Tahun kenal</label>
                          <input type="text" class="form-control" id="tahun_kenal2" name="tahun_kenal2" placeholder="Jabatan">
                        </div>

                        <hr>

                        <div class="form-group">
                          <label for="nama_refrensi3">Nama</label>
                          <input type="text" class="form-control" id="nama_refrensi3" name="nama_refrensi3" placeholder="Jabatan">
                        </div>
                        <div class="form-group">
                          <label for="alamat_refrensi3">Alamat</label>
                          <input type="text" class="form-control" id="alamat_refrensi3" name="alamat_refrensi3" placeholder="Jabatan">
                        </div>
                        <div class="form-group">
                          <label for="pekerjaan_refrensi3">Pekerjaan</label>
                          <input type="text" class="form-control" id="pekerjaan_refrensi3" name="pekerjaan_refrensi3" placeholder="Jabatan">
                        </div>
                        <div class="form-group">
                          <label for="telpon_refrensi3">Telpon</label>
                          <input type="text" class="form-control" id="telpon_refrensi3" name="telpon_refrensi3" placeholder="Jabatan">
                        </div>
                        <div class="form-group">
                          <label for="tahun_kenal3">Tahun kenal</label>
                          <input type="text" class="form-control" id="tahun_kenal3" name="tahun_kenal2" placeholder="Jabatan">
                        </div>

                        <a class="btn btn-primary" onclick="stepper.previous()">Previous</a>
                        <a class="btn btn-primary" onclick="stepper.next()">Next</a>
                      </div>

                      <div id="lainlain-part" class="content" role="tabpanel" aria-labelledby="lainlain-part-trigger">
                        <div class="form-group">
                          <label for="pertanyaan_1">Apa motivasi anda untuk bekerja?</label>
                          <input type="text" class="form-control" id="pertanyaan_1" name="pertanyaan_1" placeholder="pertanyaan_1">
                        </div>
                        <div class="form-group">
                          <label for="pertanyaan_2">Gaji dan tunjangan yang anda harapkan?</label>
                          <input type="text" class="form-control" id="pertanyaan_2" name="pertanyaan_2" placeholder="pertanyaan_2">
                        </div>
                        <div class="form-group">
                          <label for="pertanyaan_3">Apa alasan anda melaman ke perusahaan kami?</label>
                          <input type="text" class="form-control" id="pertanyaan_3" name="pertanyaan_3" placeholder="pertanyaan_3">
                        </div>
                        <div class="form-group">
                          <label for="pertanyaan_4">Bersediakah anda di tempatkan di luar daerah?</label>
                          <input type="text" class="form-control" id="pertanyaan_4" name="pertanyaan_4" placeholder="pertanyaan_4">
                        </div>
                        <div class="form-group">
                          <label for="pertanyaan_5">Kapan anda dapat mulai bekerja di perusahaan ini?</label>
                          <input type="text" class="form-control" id="pertanyaan_5" name="pertanyaan_5" placeholder="pertanyaan_5">
                        </div>
                        <div class="form-group">
                          <label for="pertanyaan_6">Apakah saudara merokok? Sudah berapa lama saudara merokok?</label>
                          <input type="text" class="form-control" id="pertanyaan_6" name="pertanyaan_6" placeholder="pertanyaan_6">
                        </div>
                        <div class="form-group">
                          <label for="pertanyaan_7">Sebutkan nama kenalan atau keluarga yang bekerja di perusahaan ini?</label>
                          <input type="text" class="form-control" id="pertanyaan_7" name="pertanyaan_7" placeholder="pertanyaan_7">
                        </div>

                        <div class="form-group">
                          <label>Pernahkah anda menderita penyakit berat, sampai dirawat dirumah sakit atau menderita penyakit yang lama sembuh?</label>
                          <select class="form-control mb-2" aria-label="Default select example" name="pilihan" id="pilihan">
                            <option selected>Tidak pernah</option>
                            <option value="1">Pernah</option>
                          </select>
                          <input type="text" class="form-control mb-2" id="jenis_penyakit" name="jenis_penyakit" placeholder="Jenis penyakit">
                          <input type="text" class="form-control mb-2" id="Tahun_berapa" name="Tahun_berapa" placeholder="Tahun berapa">
                          <input type="text" class="form-control mb-2" id="Berapa_lama" name="Berapa_lama" placeholder="Berapa lama">
                          <input type="text" class="form-control mb-2" id="Dirawat_di" name="Dirawat_di" placeholder="Dirawat di">
                        </div>

                        <div class="form-group">
                          <label>Pernahkah anda menderita penyakit yang menyebabkan hilangnya kesadaran secara tiba tiba?</label>
                          <select class="form-control mb-2" aria-label="Default select example" name="pilihan2" id="pilihan2">
                            <option selected>Tidak pernah</option>
                            <option value="1">Pernah</option>
                          </select>
                          <input type="text" class="form-control mb-2" id="jenis_penyakit2" name="jenis_penyakit2" placeholder="Jenis penyakit">
                          <input type="text" class="form-control mb-2" id="Tahun_berapa2" name="Tahun_berapa2" placeholder="Tahun berapa">
                          <input type="text" class="form-control mb-2" id="Berapa_lama2" name="Berapa_lama2" placeholder="Berapa lama">
                          <input type="text" class="form-control mb-2" id="Dirawat_di2" name="Dirawat_di2" placeholder="Dirawat di">
                        </div>

                        <a class="btn btn-primary" onclick="stepper.previous()">Previous</a>
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </div>
                  </form>



                </div>
              </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
            </div>
          </div>

        </div>
      </div>





      <!-- ================================================================================================== -->

  </div>
</div>
</div>
</section>
</div>
</div>
</a>
</a>
</a>


<script>
  // BS-Stepper Init
  document.addEventListener('DOMContentLoaded', function() {
    window.stepper = new Stepper(document.querySelector('.bs-stepper'))
  })
</script>