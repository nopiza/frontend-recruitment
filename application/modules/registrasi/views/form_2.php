<div class="content">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
            PT. Jakarta Prima Crane
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->

        <div class="row">
        <div class="col-lg-12 col-12">
          <hr>
          <br>

          <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">Form Registrasi</h3>
              </div>
              <div class="card-body p-0">
                <div class="bs-stepper linear">
                  <div class="bs-stepper-header" role="tablist">
                    <!-- your steps here -->
                    <div class="step active" data-target="#pribadi-part">
                      <button type="button" class="step-trigger" role="tab" aria-controls="pribadi-part" id="pribadi-part-trigger" aria-selected="true">
                        <span class="bs-stepper-circle">1</span>
                        <span class="bs-stepper-label">Data Pribadi</span>
                      </button>
                    </div>

                    <div class="line"></div>
                    <div class="step" data-target="#keluarga-part">
                      <button type="button" class="step-trigger" role="tab" aria-controls="keluarga-part" id="keluarga-part-trigger" aria-selected="false" disabled="disabled">
                        <span class="bs-stepper-circle">2</span>
                        <span class="bs-stepper-label">Data Keluarga</span>
                      </button>
                    </div>

                    <div class="line"></div>
                    <div class="step" data-target="#pendidikan-part">
                      <button type="button" class="step-trigger" role="tab" aria-controls="pendidikan-part" id="pendidikan-part-trigger" aria-selected="false" disabled="disabled">
                        <span class="bs-stepper-circle">3</span>
                        <span class="bs-stepper-label">Data Pendidikan</span>
                      </button>
                    </div>

                    <div class="line"></div>
                    <div class="step" data-target="#nonformal-part">
                      <button type="button" class="step-trigger" role="tab" aria-controls="nonformal-part" id="nonformal-part-trigger" aria-selected="false" disabled="disabled">
                        <span class="bs-stepper-circle">4</span>
                        <span class="bs-stepper-label">Pendidikan Non Formal</span>
                      </button>
                    </div>

                    <div class="line"></div>
                    <div class="step" data-target="#bahasa-part">
                      <button type="button" class="step-trigger" role="tab" aria-controls="bahasa-part" id="bahasa-part-trigger" aria-selected="false" disabled="disabled">
                        <span class="bs-stepper-circle">5</span>
                        <span class="bs-stepper-label">Bahasa</span>
                      </button>
                    </div>

                    <div class="line"></div>
                    <div class="step" data-target="#information-part">
                      <button type="button" class="step-trigger" role="tab" aria-controls="information-part" id="information-part-trigger" aria-selected="false" disabled="disabled">
                        <span class="bs-stepper-circle">6</span>
                        <span class="bs-stepper-label">Prestasi</span>
                      </button>
                    </div>

                    <div class="line"></div>
                    <div class="step" data-target="#information-part">
                      <button type="button" class="step-trigger" role="tab" aria-controls="information-part" id="information-part-trigger" aria-selected="false" disabled="disabled">
                        <span class="bs-stepper-circle">7</span>
                        <span class="bs-stepper-label">Keahlian</span>
                      </button>
                    </div>

                    <div class="line"></div>
                    <div class="step" data-target="#information-part">
                      <button type="button" class="step-trigger" role="tab" aria-controls="information-part" id="information-part-trigger" aria-selected="false" disabled="disabled">
                        <span class="bs-stepper-circle">8</span>
                        <span class="bs-stepper-label">Kegiatan / Hoby</span>
                      </button>
                    </div>

                    <div class="line"></div>
                    <div class="step" data-target="#information-part">
                      <button type="button" class="step-trigger" role="tab" aria-controls="information-part" id="information-part-trigger" aria-selected="false" disabled="disabled">
                        <span class="bs-stepper-circle">9</span>
                        <span class="bs-stepper-label">Pengalaman Kerja</span>
                      </button>
                    </div>

                    <div class="line"></div>
                    <div class="step" data-target="#information-part">
                      <button type="button" class="step-trigger" role="tab" aria-controls="information-part" id="information-part-trigger" aria-selected="false" disabled="disabled">
                        <span class="bs-stepper-circle">4</span>
                        <span class="bs-stepper-label">Lain - Lain</span>
                      </button>
                    </div>


                  </div>
                  <div class="bs-stepper-content">
                    <hr>
                    <!-- your steps content here -->
                    <div id="pribadi-part" class="content active dstepper-block" role="tabpanel" aria-labelledby="pribadi-part-trigger">
                    <form>
                        <div class="form-group row">
                          <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                          <div class="col-sm-10">
                            <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
                          <div class="col-sm-10">
                            <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
                          </div>
                        </div>
                        <fieldset class="form-group">
                          <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">Radios</legend>
                            <div class="col-sm-10">
                              <div class="form-check">
                                <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" checked>
                                <label class="form-check-label" for="gridRadios1">
                                  First radio
                                </label>
                              </div>
                              <div class="form-check">
                                <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="option2">
                                <label class="form-check-label" for="gridRadios2">
                                  Second radio
                                </label>
                              </div>
                              <div class="form-check disabled">
                                <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios3" value="option3" disabled>
                                <label class="form-check-label" for="gridRadios3">
                                  Third disabled radio
                                </label>
                              </div>
                            </div>
                          </div>
                        </fieldset>
                        <div class="form-group row">
                          <div class="col-sm-2">Checkbox</div>
                          <div class="col-sm-10">
                            <div class="form-check">
                              <input class="form-check-input" type="checkbox" id="gridCheck1">
                              <label class="form-check-label" for="gridCheck1">
                                Example checkbox
                              </label>
                            </div>
                          </div>
                        </div>

                      </form>
                      <button class="btn btn-primary" onclick="stepper.next()">Next</button>
                    </div>


                    <div id="keluarga-part" class="content active dstepper-block" role="tabpanel" aria-labelledby="keluarga-part-trigger">
                    <form>
                        <div class="form-group row">
                          <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                          <div class="col-sm-10">
                            <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
                          <div class="col-sm-10">
                            <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
                          </div>
                        </div>


                      </form>
                      <button class="btn btn-primary" onclick="stepper.next()">Next</button>
                    </div>


                  



                    <div id="nonformal-part" class="content" role="tabpanel" aria-labelledby="nonformal-part-trigger">
                      <div class="form-group">
                        <label for="exampleInputFile">File input</label>
                        <div class="input-group">
                          <div class="custom-file">
                            <input type="file" class="custom-file-input" id="exampleInputFile">
                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                          </div>
                          <div class="input-group-append">
                            <span class="input-group-text">Upload</span>
                          </div>
                        </div>
                      </div>
                      <button class="btn btn-primary" onclick="stepper.previous()">Previous</button>
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                Visit <a href="https://github.com/Johann-S/bs-stepper/#how-to-use-it">bs-stepper documentation</a> for more examples and information about the plugin.
              </div>
            </div>

        </div>
        </div>
          


  

<!-- ================================================================================================== -->

        </div>
      </div>
    </div>
  </section>
  </div>
</div>
</a>
</a>
</a>


<script>
    // BS-Stepper Init
    document.addEventListener('DOMContentLoaded', function () {
    window.stepper = new Stepper(document.querySelector('.bs-stepper'))
  })
</script>