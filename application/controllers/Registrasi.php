<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrasi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index($idLowongan='0')
	{
		$data['loker'] = $this->db->query("SELECT * FROM lowongan WHERE id_lowongan='$idLowongan'")->row_array();

		$this->load->view('header');
		$this->load->view('registrasi', $data);
		$this->load->view('footer');
	}

	public function proses_reg()
	{
        $kodeREG = create_random();
        $param = array(
            'no_ktp'        => $this->input->post('no_ktp'),
            'nama_lengkap' 	=> $this->input->post('nama_lengkap'),
            'tempat_lahir' 	=> $this->input->post('tempat_lahir'),
            'tanggal_lahir' => $this->input->post('tanggal_lahir'),	
            'email' 	    => $this->input->post('email'),
            'no_telp' 	    => $this->input->post('no_telp'),
            'no_wa' 	    => $this->input->post('no_wa'),
            'id_lowongan' 	=> $this->input->post('id_lowongan'),
            'kode_reg'      => $kodeREG
        );
        $this->db->insert('registrasi', $param);
        // convert no WA
        $noWA = str_replace(' ','', $this->input->post('no_wa'));
        $noWA = str_replace('-','', $noWA);
        $noWA = str_replace('.','', $noWA);
        $belakang = substr($noWA,1);
          $awal = substr($noWA,0,1);
          
          if($awal == '0'){
            $penerima = '62'.$belakang;
          }else{
            $penerima = $noWA;
          }
        // kirim Pesan
        $idLowongan = $this->input->post('id_lowongan');
        $pesan = $this->db->query("SELECT * FROM template WHERE id_template='1'")->row_array();
        $lowker = $this->db->query("SELECT * FROM lowongan WHERE id_lowongan='$idLowongan'")->row_array();

        $pesan = str_replace('[nama]', $this->input->post('nama_lengkap'), $pesan['isi_template']);
        $pesan = str_replace('[judul]', $lowker['judul_lowongan'], $pesan);

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://app.whatspie.com/api/messages',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => 'receiver='.$penerima.'&device=6281250727583&message='.$pesan.'&type=chat',
          CURLOPT_HTTPHEADER => array(
            'Accept: application/json',
            'Content-Type: application/x-www-form-urlencoded',
            'Authorization: Bearer O6KoyhPIljCfKgHYtSNIprbxVWHCQq055pw3CkRDJpClRJL5rb'
          ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);


		  redirect('registrasi/berhasil/'.$kodeREG);
	}

    public function berhasil($kodeREG){

      $data['dataReg'] = $this->db->query("SELECT * FROM registrasi WHERE kode_reg='$kodeREG'")->row_array();

      $this->load->view('header');
      $this->load->view('berhasil', $data);
      $this->load->view('footer');
	}

}
