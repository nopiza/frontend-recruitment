<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lowongan extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'cuti');

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['lowongan'] = $this->db->query("SELECT * FROM lowongan WHERE stt_lowongan ='1' ORDER BY tanggal_mulai DESC")->result();

		$this->load->view('header');
		$this->load->view('lowongan', $data);
		$this->load->view('footer');
	}

	public function detail($id)
	{
		$data['detail'] = $this->db->query("SELECT * FROM lowongan WHERE id_lowongan='$id'")->row_array();

		$this->load->view('header');
		$this->load->view('detail', $data);
		$this->load->view('footer');
	}


	

	

}
