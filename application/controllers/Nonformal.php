<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Products extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model("Kursus_model");
    $this->load->library('form_validation');
  }

  public function add()
  {
    $product = $this->product_model;
    $validation = $this->form_validation;
    $validation->set_rules($product->rules());

    if ($validation->run()) {
      $product->save();
      $this->session->set_flashdata('success', 'Berhasil disimpan');
    }

    $this->load->view("form/kursus");
  }
}
