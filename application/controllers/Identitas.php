<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller
{
  public function __construct()
    {
        parent::__construct();
        $this->load->model("Identitas_model");
        $this->load->library('form_validation');
    }

  public function add()
  {
    $identitas = $this->identitas_model;
    $validation = $this->form_validation;
    $validation->set_rules($identitas->rules());

    if ($validation->run()) {
      $identitas->save();
      $this->session->set_flashdata('success', 'Berhasil disimpan');
    }

    $this->load->view("form/identitas");
  }
}