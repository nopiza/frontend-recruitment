<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Identitas_model extends CI_Model
{
  private $_table = "identitas";

  public $hoby;
  public $nama_organisasi;
  public $jenis_organisasi;
  public $tahun;
  public $jabatan;

  public function rules()
  {
    return [
      [
        'field' => 'hoby',
        'label' => 'Hoby',
        'rules' => 'required'
      ],

      [
        'field' => 'nama_organisasi',
        'label' => 'Nama organisasi',
        'rules' => 'required'
      ],

      [
        'field' => 'jenis_organisasi',
        'label' => 'Jenis organisasi',
        'rules' => 'required'
      ],
      [
        'field' => 'tahun',
        'label' => 'Tahun',
        'rules' => 'required'
      ],
      [
        'field' => 'jabatan',
        'label' => 'Jabatan',
        'rules' => 'required'
      ],
    ];
  }

  public function save()
  {
    $post = $this->input->post();
    $this->hoby = $post["hoby"];
    $this->nama_organisasi = $post["nama_organisasi"];
    $this->jenis_organisasi = $post["jenis_organisasi"];
    $this->tahun = $post["tahun"];
    $this->jabatan = $post["jabatan"];
    return $this->db->insert($this->_table, $this);
  }
}
