<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Identitas_model extends CI_Model
{
  private $_table = "identitas";

  public $bahasa;
  public $berbicara;
  public $membaca;
  public $menulis;

  public function rules()
  {
    return [
      [
        'field' => 'bahasa',
        'label' => 'Bahasa',
        'rules' => 'required'
      ],

      [
        'field' => 'berbicara',
        'label' => 'Berbicara',
        'rules' => 'required'
      ],

      [
        'field' => 'membaca',
        'label' => 'Membaca',
        'rules' => 'required'
      ],
      [
        'field' => 'menulis',
        'label' => 'Menulis',
        'rules' => 'required'
      ],
    ];
  }

  public function save()
  {
    $post = $this->input->post();
    $this->bahasa = $post["bahasa"];
    $this->berbicara = $post["berbicara"];
    $this->membaca = $post["membaca"];
    $this->menulis = $post["menulis"];
    return $this->db->insert($this->_table, $this);
  }
}
