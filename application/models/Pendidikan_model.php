<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Identitas_model extends CI_Model
{
  private $_table = "identitas";

  public $sekolah;
  public $dari;
  public $sampai;
  public $jurusan;
  public $tempat;
  public $ijazah;
  public $keterangan;

  public function rules()
  {
    return [
      [
        'field' => 'sekolah',
        'label' => 'Sekolah',
        'rules' => 'required'
      ],

      [
        'field' => 'dari',
        'label' => 'Dari',
        'rules' => 'required'
      ],

      [
        'field' => 'sampai',
        'label' => 'Sampai',
        'rules' => 'required'
      ],
      [
        'field' => 'jurusan',
        'label' => 'Jurusan',
        'rules' => 'required'
      ],
      [
        'field' => 'tempat',
        'label' => 'Tempat',
        'rules' => 'required'
      ],
      [
        'field' => 'ijazah',
        'label' => 'Ijazah',
        'rules' => 'required'
      ],
      [
        'field' => 'keterangan',
        'label' => 'Keterangan',
        'rules' => 'required'
      ],
    ];
  }

  public function save()
  {
    $post = $this->input->post();
    $this->sekolah = $post["sekolah"];
    $this->dari = $post["dari"];
    $this->sampai = $post["sampai"];
    $this->jurusan = $post["jurusan"];
    $this->tempat = $post["tempat"];
    $this->ijazah = $post["ijazah"];
    $this->keterangan = $post["keterangan"];
    return $this->db->insert($this->_table, $this);
  }
}
