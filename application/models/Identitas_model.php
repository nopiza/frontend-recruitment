<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Identitas_model extends CI_Model
{
  private $_table = "identitas";

  public $id;
  public $nama_lengkap;
  public $nama_panggilan;
  public $tempat_tgl_lahir;
  public $jenis_kelamin;
  public $alamat_sesuai_ktp;
  public $kode_pos_ktp;
  public $alamat_sekarang;
  public $kode_pos_alamat;
  public $no_telp_pribadi;
  public $no_telp_istri;
  public $agama;
  public $suku_bangsa;
  public $kewarganegaraan;
  public $status;
  public $no_ktp;
  public $masa_berlaku_ktp;
  public $no_npwp;
  public $no_bpjs;
  public $tanggal_terbit_bpjs;
  public $no_jaminan_pensiun;
  public $tanggal_terbit_jaminan;
  public $sim;
  public $masa_berlaku_sim;
  public $golongan_darah;
  public $ukuran_baju;
  public $berat_badan;
  public $ukuran_sepatu;
  public $tinggi_badan;
  public $email;

  public function rules()
  {
    return [
      [
        'field' => 'nama_lengkap',
        'label' => 'Nama lengkap',
        'rules' => 'required'
      ],

      [
        'field' => 'nama_panggilan',
        'label' => 'Nama panggilan',
        'rules' => 'required'
      ],

      [
        'field' => 'tempat_tgl_lahir',
        'label' => 'Tempat tanggal lahir',
        'rules' => 'required'
      ],
      [
        'field' => 'jenis_kelamin',
        'label' => 'Jenis kelamin',
        'rules' => 'required'
      ],
      [
        'field' => 'alamat_sesuai_ktp',
        'label' => 'Alamat sesuai ktp',
        'rules' => 'required'
      ],
      [
        'field' => 'kode_pos_ktp',
        'label' => 'Kode pos ktp',
        'rules' => 'numeric|required'
      ],
      [
        'field' => 'alamat_sekarang',
        'label' => 'Alamat sekarang',
        'rules' => 'required'
      ],
      [
        'field' => 'kode_pos_alamat',
        'label' => 'Kode pos alamat',
        'rules' => 'numeric|required'
      ],
      [
        'field' => 'no_telp_pribadi',
        'label' => 'No telp pribadi',
        'rules' => 'numeric|required'
      ],
      [
        'field' => 'no_telp_istri',
        'label' => 'No telp istri',
        'rules' => 'numeric'
      ],
      [
        'field' => 'agama',
        'label' => 'Agama',
        'rules' => 'required'
      ],
      [
        'field' => 'suku_bangsa',
        'label' => 'Suku bangsa',
        'rules' => 'required'
      ],
      [
        'field' => 'kewarganegaraan',
        'label' => 'Kewarganegaraan',
        'rules' => 'required'
      ],
      [
        'field' => 'status',
        'label' => 'status',
        'rules' => 'required'
      ],
      [
        'field' => 'no_ktp',
        'label' => 'No ktp',
        'rules' => 'numeric|required'
      ],
      [
        'field' => 'masa_berlaku_ktp',
        'label' => 'Masa berlaku ktp',
        'rules' => 'required'
      ],
      [
        'field' => 'no_npwp',
        'label' => 'No npwp',
        'rules' => 'numeric|required'
      ],
      [
        'field' => 'no_bpjs',
        'label' => 'No bpjs',
        'rules' => 'numeric|required'
      ],
      [
        'field' => 'tanggal_terbit_bpjs',
        'label' => 'Tanggal terbit bpjs',
        'rules' => 'required'
      ],
      [
        'field' => 'no_jaminan_pensiun',
        'label' => 'No jaminan pensiun',
        'rules' => 'numeric|required'
      ],
      [
        'field' => 'tanggal_terbit_jaminan',
        'label' => 'Tanggal terbit jaminan',
        'rules' => 'required'
      ],
      [
        'field' => 'sim',
        'label' => 'Sim',
        'rules' => 'numeric|required'
      ],
      [
        'field' => 'masa_berlaku_sim',
        'label' => 'Masa berlaku sim',
        'rules' => 'required'
      ],
      [
        'field' => 'golongan_darah',
        'label' => 'Golongan darah',
        'rules' => 'required'
      ],
      [
        'field' => 'ukuran_baju',
        'label' => 'Ukuran baju',
        'rules' => 'required'
      ],
      [
        'field' => 'berat_badan',
        'label' => 'Berat badan',
        'rules' => 'numeric|required'
      ],
      [
        'field' => 'ukuran_sepatu',
        'label' => 'Ukuran sepatu',
        'rules' => 'numeric|required'
      ],
      [
        'field' => 'tinggi_badan',
        'label' => 'Tinggi badan',
        'rules' => 'numeric|required'
      ],
      [
        'field' => 'email',
        'label' => 'email',
        'rules' => 'required|valid_email'
      ],
    ];
  }

  public function save()
  {
    $post = $this->input->post();
    $this->nama_lengkap = $post["nama_lengkap"];
    $this->nama_panggilan = $post["nama_panggilan"];
    $this->tempat_tgl_lahir = $post["tempat_tgl_lahir"];
    $this->jenis_kelamin = $post["jenis_kelamin"];
    $this->alamat_sesuai_ktp = $post["alamat_sesuai_ktp"];
    $this->kode_pos_ktp = $post["kode_pos_ktp"];
    $this->alamat_sesuai_ktp = $post["alamat_sesuai_ktp"];
    $this->kode_pos_ktp = $post["kode_pos_ktp"];
    $this->alamat_sekarang = $post["alamat_sekarang"];
    $this->kode_pos_alamat = $post["kode_pos_alamat"];
    $this->no_telp_pribadi = $post["no_telp_pribadi"];
    $this->no_telp_istri = $post["no_telp_istri"];
    $this->agama = $post["agama"];
    $this->suku_bangsa = $post["suku_bangsa"];
    $this->kewarganegaraan = $post["kewarganegaraan"];
    $this->status = $post["status"];
    $this->no_ktp = $post["no_ktp"];
    $this->masa_berlaku_ktp = $post["masa_berlaku_ktp"];
    $this->no_npwp = $post["no_npwp"];
    $this->no_bpjs = $post["no_bpjs"];
    $this->tanggal_terbit_bpjs = $post["tanggal_terbit_bpjs"];
    $this->no_jaminan_pensiun = $post["no_jaminan_pensiun"];
    $this->tanggal_terbit_bpjs = $post["tanggal_terbit_bpjs"];
    $this->no_jaminan_pensiun = $post["no_jaminan_pensiun"];
    $this->tanggal_terbit_bpjs = $post["tanggal_terbit_bpjs"];
    $this->no_jaminan_pensiun = $post["no_jaminan_pensiun"];
    $this->tanggal_terbit_jaminan = $post["tanggal_terbit_jaminan"];
    $this->sim = $post["sim"];
    $this->masa_berlaku_sim = $post["masa_berlaku_sim"];
    $this->golongan_darah = $post["golongan_darah"];
    $this->ukuran_baju = $post["ukuran_baju"];
    $this->berat_badan = $post["berat_badan"];
    $this->ukuran_sepatu = $post["ukuran_sepatu"];
    $this->tinggi_badan = $post["tinggi_badan"];
    $this->email = $post["email"];
    return $this->db->insert($this->_table, $this);
  }
}