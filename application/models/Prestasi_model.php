<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Identitas_model extends CI_Model
{
  private $_table = "identitas";

  public $kejuaraan;
  public $bidang;
  public $penyelenggara;
  public $rengking;
  public $keterangan;

  public function rules()
  {
    return [
      [
        'field' => 'kejuaraan',
        'label' => 'Kejuaraan',
        'rules' => 'required'
      ],

      [
        'field' => 'bidang',
        'label' => 'Bidang',
        'rules' => 'required'
      ],

      [
        'field' => 'penyelenggara',
        'label' => 'Penyelenggara',
        'rules' => 'required'
      ],
      [
        'field' => 'rengking',
        'label' => 'Rengking',
        'rules' => 'required'
      ],
      [
        'field' => 'keterangan',
        'label' => 'Keterangan',
        'rules' => 'required'
      ],
    ];
  }

  public function save()
  {
    $post = $this->input->post();
    $this->kejuaraan = $post["kejuaraan"];
    $this->bidang = $post["bidang"];
    $this->penyelenggara = $post["penyelenggara"];
    $this->rengking = $post["rengking"];
    $this->keterangan = $post["keterangan"];
    return $this->db->insert($this->_table, $this);
  }
}
