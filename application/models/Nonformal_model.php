<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Identitas_model extends CI_Model
{
  private $_table = "identitas";

  public $pelatihan;
  public $tahun;
  public $penyelenggara;
  public $tempat;
  public $sertifikat;
  public $keterangan;

  public function rules()
  {
    return [
      [
        'field' => 'pelatihan',
        'label' => 'Pelatihan',
        'rules' => 'required'
      ],

      [
        'field' => 'tahun',
        'label' => 'Tahun',
        'rules' => 'required'
      ],

      [
        'field' => 'penyelenggara',
        'label' => 'Penyelenggara',
        'rules' => 'required'
      ],
      [
        'field' => 'tempat',
        'label' => 'Tempat',
        'rules' => 'required'
      ],
      [
        'field' => 'sertifikat',
        'label' => 'Sertifikat',
        'rules' => 'required'
      ],
      [
        'field' => 'keterangan',
        'label' => 'Keterangan',
        'rules' => 'required'
      ],
      [
        'field' => 'keterangan',
        'label' => 'Keterangan',
        'rules' => 'required'
      ],
    ];
  }

  public function save()
  {
    $post = $this->input->post();
    $this->pelatihan = $post["pelatihan"];
    $this->tahun = $post["tahun"];
    $this->penyelenggara = $post["penyelenggara"];
    $this->tempat = $post["tempat"];
    $this->sertifikat = $post["sertifikat"];
    $this->keterangan = $post["keterangan"];
    return $this->db->insert($this->_table, $this);
  }
}
