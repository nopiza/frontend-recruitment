<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Identitas_model extends CI_Model
{
  private $_table = "identitas";

  public $nama_keahlian;
  public $tingkat_keahlian;
  public $sertifikat;

  public function rules()
  {
    return [
      [
        'field' => 'nama_keahlian',
        'label' => 'Nama keahlian',
        'rules' => 'required'
      ],

      [
        'field' => 'tingkat_keahlian',
        'label' => 'Tingkat keahlian',
        'rules' => 'required'
      ],

      [
        'field' => 'sertifikat',
        'label' => 'Sertifikat',
        'rules' => 'required'
      ],
    ];
  }

  public function save()
  {
    $post = $this->input->post();
    $this->nama_keahlian = $post["nama_keahlian"];
    $this->tingkat_keahlian = $post["tingkat_keahlian"];
    $this->sertifikat = $post["sertifikat"];
    return $this->db->insert($this->_table, $this);
  }
}
