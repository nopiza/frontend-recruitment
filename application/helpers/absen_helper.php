<?php

function cek_mesin($kolok){
    $CI = &get_instance();
    $cekolok = $CI->db->query("SELECT * FROM master_skpd WHERE kolok='$kolok'")->row_array();

    $pakai = $cekolok['STT_PEMAKAI'];

    return $pakai;
}

// function hadir(){
//     $CI = &get_instance();
//     $absen = $CI->db->query("SELECT SUM(jum) as hadir FROM (
//         SELECT COUNT(id_absensi) as jum FROM `absensi` WHERE tanggal_absensi like '2022-06-07%'
//         UNION
//         SELECT COUNT(sn) as jum FROM `att_log` WHERE scan_date like '2022-06-07%'
//         UNION
//         SELECT COUNT(id) as jum FROM `tabsenlog` WHERE tgl = '2022-06-07'
//         ) as tmp;
//     ")->row_array();
//     $total = $absen['hadir'];
//     return $total;
// }

function hadirMaster($tanggal = "", $kolok=""){
    $CI = &get_instance();
    $kolokPegawai = "";
    $kolokMesin = "";
    if($kolok != ''){
        $kolokMesin = " AND m.kolok='$kolok'";
        $kolokPegawai = " AND p.kode_skpd='$kolok'";
    }else{
        $kolokPegawai = "";
        $kolokMesin = "";
    }
    $absen = $CI->db->query("SELECT COUNT(*) as hadir FROM (

    SELECT sn, pin as idAbsen, scan_date as tang, kolok, nip  FROM att_log a 
    LEFT JOIN mesin m ON a.sn = m.sn_mesin 
    LEFT JOIN mapping_id_absen p ON a.sn = p.sn_mesin AND a.pin=p.id_absen
    WHERE a.scan_date like '$tanggal%' $kolokMesin

    UNION

    SELECT  '' as sn, id_account as idAbsen, tanggal_absensi as tang, kode_skpd as kolok, nip FROM absensi a 
    LEFT JOIN pegawai p ON a.id_account=p.id_pegawai 
    WHERE tanggal_absensi like '$tanggal%' $kolokPegawai

    UNION

    SELECT  sn, idcard as idAbsen, concat(tgl,' ',waktu) as tang, kolok, nip FROM tabsenlog a 
    LEFT JOIN mesin m ON a.sn = m.sn_mesin 
    LEFT JOIN mapping_id_absen p ON a.sn = p.sn_mesin AND a.idcard=p.id_absen
    WHERE tgl = '$tanggal%' $kolokMesin

    ) as tmp WHERE nip != 'NULL' AND nip NOT LIKE '%PLT%' GROUP BY nip;
    ")->num_rows();
    // $total = $absen['hadir'];
    return $absen;
}

function jamMasukHarini($tanggal){
    $CI = &get_instance();
    $hari = strtolower(namaHari($tanggal));
    $jamKerja = $CI->db->query("SELECT * FROM jam_kerja WHERE status='1'")->row_array();

    $jamKer['masuk'] = $jamKerja[$hari.'_m'];
    $jamKer['batas'] = $jamKerja[$hari.'_batas'];
    $jamKer['pulang'] = $jamKerja[$hari.'_p'];
    return $jamKer;
}


function DashTerlambat($tanggal, $kolok=""){
    $CI = &get_instance();
    $sekarang = tgl_now();
    if($kolok !=''){
        $queryKolokMesin = " AND m.kolok='$kolok'";
        $queryKolokPegawai = " AND p.kode_skpd='$kolok'";
    }else{
        $queryKolokMesin = '';
        $queryKolokPegawai = '';
    }

    //query data pegawai terlambat seluruh kota
		$jamMAsuk = $sekarang.' '.jamMasukHarini(tgl_now())['masuk'];
		$jamBatas = $sekarang.' '.jamMasukHarini(tgl_now())['batas'];
		$total = $CI->db->query("SELECT p.nama_pegawai, p.nip, jd.tang, jd.tanggal, jd.jenis, p.status_kepegawaian FROM pegawai p 
		LEFT JOIN (SELECT pegawai.nip, tmp.tang, MIN(tang) as tanggal, tmp.jenis FROM pegawai 
				   
		LEFT JOIN (SELECT sn, pin as idAbsen, scan_date as tang, kolok, nip, 'FingerSpot' as jenis FROM att_log a 
				   LEFT JOIN mesin m ON a.sn = m.sn_mesin LEFT JOIN mapping_id_absen p ON a.sn = p.sn_mesin AND a.pin=p.id_absen 
				   WHERE a.scan_date like '$sekarang%' $queryKolokMesin 
				   UNION 
				   SELECT '' as sn, id_account as idAbsen, tanggal_absensi as tang, kode_skpd as kolok, nip, 'Aplikasi' as jenis FROM absensi a 
				   LEFT JOIN pegawai p ON a.id_account=p.id_pegawai 
				   WHERE tanggal_absensi like '$sekarang%' $queryKolokPegawai
				  ) tmp ON pegawai.nip = tmp.nip  GROUP BY tmp.nip
		) jd ON p.nip = jd.nip WHERE jd.tanggal < '$jamBatas' AND jd.tanggal > '$jamMAsuk';")->num_rows();

    return $total;
}

function hitungpotognan($nip, $bulan){
    $CI = &get_instance();

    $namatable_hitung   = 'hitung_'.$bulan.'_2021';

    // perulangan hari
    $pegawai = $CI->db->query("SELECT * FROM $namatable_hitung WHERE nip='$nip'")->result();
    $Gpot = 0;
    foreach ($pegawai as $pgw) {
        // $potharian = '';
        $cekpot = $CI->db->query("SELECT * FROM $namatable_hitung WHERE TANGGAL='$pgw->TANGGAL'")->row_array();

        if(($cekpot['POTONGANMSK'] + $cekpot['POTONGANPLNG']) > 5 ){
            $pot = 5;
        }else{
            $pot = $cekpot['POTONGANMSK'] + $cekpot['POTONGANPLNG'];
        }

        $Gpot = $Gpot + $pot;
    }

    

    return $Gpot;
}


function cariJamMasuk($tanggal, $id) {
    //cari hari 
    $hari = namaHari($tanggal);
    if($hari == 'Jumat'){
        $batas = $tanggal.' 10:00:00';
    }else{
        $batas = $tanggal.' 12:00:00';
    }
    
    $CI = &get_instance();
    $absen = $CI->db->query("
     --    SELECT MAX(tanggal_absensi) as masuk FROM absensi
     -- WHERE tanggal_absensi like '$tanggal%' 
     -- AND id_account='$id' 
     -- AND type_absensi = 'MASUK' 

     SELECT MAX(tanggal_absensi) as masuk FROM absensi
     WHERE tanggal_absensi like '$tanggal%' 
     AND id_account='$id' 
     AND type_absensi = 'MASUK' HAVING masuk is NOT NULL

     union all

     SELECT MAX(tanggal_absensi) as masuk FROM absensi_mesin
     WHERE tanggal_absensi like '$tanggal%' 
     AND id_account='$id' 
     AND type_absensi = 'MASUK' HAVING masuk is NOT NULL

     ")->row_array();

    if(@$absen['masuk'] == ''){
        $masuk = '-';
    }else{
        $jenis = "MASUK";
        $pecah = explode(' ', $absen['masuk']);
        $masuk = $pecah[1].'<br>
        <button class="btn btn-primary btn-xs" onclick="detail('.$id.', '.'\'MASUK\''.', '.'\''.$tanggal.'\''.')">detail</button>';
    }
    
    return $masuk;
}


function cariJamPulang($tanggal, $id) {
    $batas = $tanggal.' 12:00:00';
    $CI = &get_instance();
    $absen = $CI->db->query("
     --    SELECT MAX(tanggal_absensi) as pulang FROM absensi
     -- WHERE tanggal_absensi like '$tanggal%' 
     -- AND id_account='$id' 
     -- AND type_absensi = 'PULANG'

     SELECT MAX(tanggal_absensi) as pulang  FROM absensi
     WHERE tanggal_absensi like '$tanggal%' 
     AND id_account='$id' 
     AND type_absensi = 'PULANG'  HAVING pulang is NOT NULL

     union all

     SELECT MAX(tanggal_absensi) as pulang  FROM absensi_mesin
     WHERE tanggal_absensi like '$tanggal%' 
     AND id_account='$id' 
     AND type_absensi = 'PULANG'  HAVING pulang is NOT NULL

     ")->row_array();
    if(@$absen['pulang'] == ''){
        $pulang = '-';
    }else{
        $pecah = explode(' ', $absen['pulang']);
        $pulang = $pecah[1].'<br>
        <button class="btn btn-primary btn-xs" onclick="detail('.$id.', '.'\'PULANG\''.', '.'\''.$tanggal.'\''.')">detail</button>';
    }
    
    return $pulang;
}



///Ini khusus untul laporan admin / tanpa tambahan tombol

function cariJamMasuk_adm($tanggal, $id) {
    //cari hari 
    $hari = namaHari($tanggal);
    if($hari == 'Jumat'){
        $batas = $tanggal.' 10:00:00';
    }else{
        $batas = $tanggal.' 12:00:00';
    }
    
    $CI = &get_instance();
    $absen = $CI->db->query("


     SELECT MAX(tanggal_absensi) as masuk FROM absensi
     WHERE tanggal_absensi like '$tanggal%' AND tanggal_absensi < '$batas' 
     AND id_account='$id' 
     AND type_absensi = 'MASUK' HAVING masuk is NOT NULL

     union all

     SELECT MAX(tanggal_absensi) as masuk FROM absensi_mesin
     WHERE tanggal_absensi like '$tanggal%' 
     AND id_account='$id' 
     AND type_absensi = 'MASUK' HAVING masuk is NOT NULL

     ")->row_array();

    if(@$absen['masuk'] == ''){
        $masuk = '';
    }else{
        $jenis = "MASUK";
        $pecah = explode(' ', $absen['masuk']);
        $masuk = $pecah[1];
    }
    
    return $masuk;
}


function cariJamPulang_adm($tanggal, $id) {
    $batas = $tanggal.' 12:00:00';
    $CI = &get_instance();
    $absen = $CI->db->query("


     SELECT MAX(tanggal_absensi) as pulang  FROM absensi
     WHERE tanggal_absensi like '$tanggal%'  AND tanggal_absensi > '$batas' 
     AND id_account='$id' 
     AND type_absensi = 'PULANG'  HAVING pulang is NOT NULL

     union all

     SELECT MAX(tanggal_absensi) as pulang  FROM absensi_mesin
     WHERE tanggal_absensi like '$tanggal%' 
     AND id_account='$id' 
     AND type_absensi = 'PULANG'  HAVING pulang is NOT NULL

     ")->row_array();
    if(@$absen['pulang'] == ''){
        $pulang = '';
    }else{
        $pecah = explode(' ', $absen['pulang']);
        $pulang = $pecah[1];
    }
    
    return $pulang;
}


// function absenMasuk($tanggal, $id, $id_absen_mesin = '') {
//     // cari hari 
//     $hari = namaHari($tanggal);
//     if($hari == 'Jumat'){
//         $batas = $tanggal.' 10:00:00';
//     }else{
//         $batas = $tanggal.' 12:00:00';
//     }
    
//     $CI = &get_instance();
//     $pegawai = $CI->db->get_where('pegawai',array('id_pegawai' => $id))->row_array();
//     $absen = $CI->db->query("
//     SELECT tanggal_absensi, 'WFH App' as jenis FROM absensi
//      WHERE tanggal_absensi like '$tanggal%' 
//      AND id_account='$id' 
//      AND type_absensi = 'MASUK'

//      union all

//      SELECT MIN(tanggal_absensi), 'Mesin' as jenis FROM absensi_mesin
//      WHERE tanggal_absensi like '$tanggal%' AND tanggal_absensi < '$batas' 
//      AND id_account='$id' 
//      AND type_absensi = 'MASUK'

//      ")->row_array();
//     if(@$absen['tanggal_absensi'] == ''){
//         $masuk = '-';
//     }else{
//         $jenis = "MASUK";
//         $pecah = explode(' ', $absen['tanggal_absensi']);
//         if($absen['jenis'] == 'Mesin'){
//             $masuk = $pecah[1].' <button class="btn btn-primary btn-xs" >'.$absen['jenis'].'</button>';
//         }else{
//             $masuk = $pecah[1].' <button class="btn btn-warning btn-xs" >'.$absen['jenis'].'</button>';
//         }
        
//     }
    
//     return $masuk;
// }



function absenMasuk_new($tanggal, $id, $nip = '', $tombol = '') {
    // cari hari 
    $hari = namaHari($tanggal);
    if($hari == 'Jumat'){
        $batas = $tanggal.' 10:00:00';
    }else{
        $batas = $tanggal.' 12:00:00';
    }
    
    $CI = &get_instance();
    $pegawai = $CI->db->get_where('karyawan',array('id_karyawan' => $id))->row_array();
    // $kodeSKPD = $pegawai['kolok'];
    

    // cari PIN mesin berdasarkan merek mesin
    
    // $idMesin_F = $CI->db->query("SELECT * FROM mapping_id_absen WHERE nip = '$nip' AND merek_mesin = 'FingerSpot'")->row_array();
    // $id_absen_mesin_F = @$idMesin_F['id_absen']; 

    // $cekSolution = $CI->db->query("SELECT * FROM mapping_id_absen WHERE nip = '$nip' AND merek_mesin = 'Solution'")->num_rows();
    
    // if($cekSolution){
    //     $idMesin_S = $CI->db->query("SELECT * FROM mapping_id_absen WHERE nip = '$nip' AND merek_mesin = 'Solution'")->row_array();
    //     $id_absen_mesin_S = $idMesin_S['id_absen']; 
    // }else{
    //     $id_absen_mesin_S = 'xxx';
    // }
    //-------------------------------------------- end

    $query = "SELECT MIN(pertama) as masuk, jenis FROM 
    (

    SELECT MIN(tanggal_absensi) as pertama, id_account as idAbsen, 'Aplikasi' as jenis  FROM absensi 
    WHERE id_account='$id' AND tanggal_absensi like '%$tanggal%' AND tanggal_absensi < '$batas' 

    ) as pagi WHERE pagi.pertama = pagi.pertama";

    $absen = $CI->db->query($query)->row_array();

    // var_dump($absen['pertama']);

    if(@$absen['masuk'] == ''){
        $masuk = '-';
    }else{
        $jenis = "MASUK";
        $pecah = explode(' ', $absen['masuk']);
        $jum = count($pecah);
        if($jum == 1){
            $jam = $pecah[0];
        }else{
            $jam = $pecah[1];
        }

        if($tombol == ''){
            if($absen['jenis'] == 'Aplikasi'){
                $masuk = $jam.' <button class="btn btn-primary btn-xs"  onclick="detail('.$id.', '.'\'MASUK\''.', '.'\''.$tanggal.'\''.')">'.$absen['jenis'].'</button>';
            }else{
                $masuk = $jam.' <button class="btn btn-warning btn-xs" >'.$absen['jenis'].'</button>';
            }
        }else{
            $masuk = $jam;
        }
        
        
    }
    
    return $masuk;
}


function absenMasuk_jpc($tanggal, $id) {
    
    $CI = &get_instance();
    $query = "SELECT MIN(tanggal_absensi) as pertama, id_account as idAbsen, type_absensi FROM absensi 
    WHERE id_account='$id' AND tanggal_absensi like '%$tanggal%' AND type_absensi = 'MASUK'";

    $absen = $CI->db->query($query)->row_array();

    if(@$absen['pertama'] == ''){
        $masuk = '-';
    }else{
        $jenis = "MASUK";
        $pecah = explode(' ', $absen['pertama']);
        $jum = count($pecah);
        if($jum == 1){
            $jam = $pecah[0];
        }else{
            $jam = $pecah[1];
        }

        $masuk = $jam;
    }
    
    return $masuk;
}


function overMasuk_jpc($tanggal, $id) {
    
    $CI = &get_instance();
    $query = "SELECT MIN(tanggal_absensi) as pertama, id_account as idAbsen, type_absensi FROM absensi 
    WHERE id_account='$id' AND tanggal_absensi like '%$tanggal%' AND type_absensi = 'MASUK LEMBUR'";

    $absen = $CI->db->query($query)->row_array();

    if(@$absen['pertama'] == ''){
        $masuk = '-';
    }else{
        $jenis = "MASUK";
        $pecah = explode(' ', $absen['pertama']);
        $jum = count($pecah);
        if($jum == 1){
            $jam = $pecah[0];
        }else{
            $jam = $pecah[1];
        }

        $masuk = $jam;
    }
    
    return $masuk;
}



function absenPulang_new($tanggal, $id, $nip = '', $tombol = '') {
    //cari hari 
    $hari = namaHari($tanggal);
    if($hari == 'Jumat'){
        $batas = $tanggal.' 10:00:00';
        $batasjam = ' 10:00:00';
    }else{
        $batas = $tanggal.' 12:00:00';
        $batasjam = ' 12:00:00';
    }
    
    $CI = &get_instance();
    $pegawai = $CI->db->get_where('karyawan',array('id_karyawan' => $id))->row_array();
   

    $query = "SELECT MAX(terakhir) as pulang, jenis FROM 
    (

    SELECT MAX(tanggal_absensi) as terakhir, id_account as idAbsen, 'Aplikasi' as jenis FROM absensi 
    WHERE id_account='$id' AND tanggal_absensi like '%$tanggal%' AND tanggal_absensi > '$batas' 

    ) as sore WHERE sore.terakhir = sore.terakhir";
    $absen = $CI->db->query($query)->row_array();

    if(@$absen['pulang'] == ''){
        $pulang = '-';
    }else{
        $jenis = "PULANG";
        $pecah = explode(' ', $absen['pulang']);
        $jum = count($pecah);
        if($jum == 1){
            $jam = $pecah[0];
        }else{
            $jam = $pecah[1];
        }

        if($tombol == ''){
            if($absen['jenis'] == 'Aplikasi'){
                $pulang = $jam.' <button class="btn btn-primary btn-xs" onclick="detail('.$id.', '.'\'MASUK\''.', '.'\''.$tanggal.'\''.')">'.$absen['jenis'].'</button>';
            }else{
                $pulang = $jam.' <button class="btn btn-warning btn-xs" >'.$absen['jenis'].'</button>';
            }
        }else{
            $pulang = $jam;
        }
        // $pulang = $pecah[1].' <button class="btn btn-secondary btn-xs" >'.$absen['jenis'].'</button>';
    }
    
    return $pulang;
}



function absenPulang_jpc($tanggal, $id) {

    $CI = &get_instance();
    $query = "SELECT MAX(tanggal_absensi) as terakhir, id_account as idAbsen, 'Aplikasi' as jenis FROM absensi 
    WHERE id_account='$id' AND tanggal_absensi like '%$tanggal%' AND type_absensi = 'PULANG'";
    $absen = $CI->db->query($query)->row_array();

    if(@$absen['terakhir'] == ''){
        $pulang = '-';
    }else{
        $jenis = "PULANG";
        $pecah = explode(' ', $absen['terakhir']);
        $jum = count($pecah);
        if($jum == 1){
            $jam = $pecah[0];
        }else{
            $jam = $pecah[1];
        }
        $pulang = $jam;
    }
    
    return $pulang;
}



function overPulang_jpc($tanggal, $id) {

    $CI = &get_instance();
    $query = "SELECT MAX(tanggal_absensi) as terakhir, id_account as idAbsen, 'Aplikasi' as jenis FROM absensi 
    WHERE id_account='$id' AND tanggal_absensi like '%$tanggal%' AND type_absensi = 'PULANG LEMBUR'";
    $absen = $CI->db->query($query)->row_array();

    if(@$absen['terakhir'] == ''){
        $pulang = '-';
    }else{
        $jenis = "PULANG";
        $pecah = explode(' ', $absen['terakhir']);
        $jum = count($pecah);
        if($jum == 1){
            $jam = $pecah[0];
        }else{
            $jam = $pecah[1];
        }
        $pulang = $jam;
    }
    
    return $pulang;
}


// function absenMasukData($tanggal, $id) {
//     //cari hari 
//     // $hari = namaHari($tanggal);
//     // if($hari == 'Jumat'){
//     //     $batas = $tanggal.' 10:00:00';
//     // }else{
//     //     $batas = $tanggal.' 12:00:00';
//     // }
    
//     $CI = &get_instance();
//     $absen = $CI->db->query("
//     SELECT tanggal_absensi, 'WFH App' as jenis FROM absensi
//      WHERE tanggal_absensi like '$tanggal%' 
//      AND id_account='$id' 
//      AND type_absensi = 'MASUK'

//      union all

//      SELECT tanggal_absensi, 'Mesin' as jenis FROM absensi_mesin
//      WHERE tanggal_absensi like '$tanggal%' 
//      AND id_account='$id' 
//      AND type_absensi = 'MASUK'




//      ")->row_array();
//     if(@$absen['tanggal_absensi'] == ''){
//         $masuk = '-';
//     }else{
//         $jenis = "MASUK";
//         $pecah = explode(' ', $absen['tanggal_absensi']);
//         $masuk = $pecah[1];
//     }
    
//     return $masuk;
// }


// function absenPulang($tanggal, $id, $id_absen_mesin = '') {
//     //cari hari 
//     $hari = namaHari($tanggal);
//     if($hari == 'Jumat'){
//         $batas = $tanggal.' 10:00:00';
//     }else{
//         $batas = $tanggal.' 12:00:00';
//     }
    
//     $CI = &get_instance();
//     $pegawai = $CI->db->get_where('pegawai',array('id_pegawai' => $id))->row_array();
//     $absen = $CI->db->query("
//     SELECT tanggal_absensi, 'WFH App' as jenis FROM absensi
//      WHERE tanggal_absensi like '$tanggal%' 
//      AND id_account='$id' 
//      AND type_absensi = 'PULANG'

//      union all

//      SELECT MAX(tanggal_absensi), 'Mesin' as jenis FROM absensi_mesin
//      WHERE tanggal_absensi like '$tanggal%'  AND tanggal_absensi > '$batas' 
//      AND id_account='$id' 
//      AND type_absensi = 'PULANG'


//      ")->row_array();
//     if(@$absen['tanggal_absensi'] == ''){
//         $pulang = '-';
//     }else{
//         $jenis = "MASUK";
//         $pecah = explode(' ', $absen['tanggal_absensi']);
//         if($absen['jenis'] == 'Mesin'){
//             $pulang = $pecah[1].' <button class="btn btn-primary btn-xs" >'.$absen['jenis'].'</button>';
//         }else{
//             $pulang = $pecah[1].' <button class="btn btn-warning btn-xs" >'.$absen['jenis'].'</button>';
//         }
        
//         // $pulang = $pecah[1].' <button class="btn btn-secondary btn-xs" >'.$absen['jenis'].'</button>';
//     }
    
//     return $pulang;
// }


// function absenPulangData($tanggal, $id) {
//     //cari hari 
//     // $hari = namaHari($tanggal);
//     // if($hari == 'Jumat'){
//     //     $batas = $tanggal.' 10:00:00';
//     // }else{
//     //     $batas = $tanggal.' 12:00:00';
//     // }
    
//     $CI = &get_instance();
//     $absen = $CI->db->query("
//     SELECT tanggal_absensi, 'WFH App' as jenis FROM absensi
//      WHERE tanggal_absensi like '$tanggal%' 
//      AND id_account='$id' 
//      AND type_absensi = 'PULANG'

//      union all

//      SELECT tanggal_absensi, 'Mesin' as jenis FROM absensi_mesin
//      WHERE tanggal_absensi like '$tanggal%' 
//      AND id_account='$id' 
//      AND type_absensi = 'PULANG'




//      ")->row_array();
//     if(@$absen['tanggal_absensi'] == ''){
//         $pulang = '-';
//     }else{
//         $jenis = "MASUK";
//         $pecah = explode(' ', $absen['tanggal_absensi']);
//         $pulang = $pecah[1];
//     }
    
//     return $pulang;
// }



// function poterlambat($jadwal, $jamAbsen){
//     $awal  = strtotime($jadwal);
//     $akhir = strtotime($jamAbsen);
//     $diff  = $akhir - $awal;
//     $menit = floor($diff / 60);
//     $pot= '';

//     if($jamAbsen === '00:00:00'){
//         $pot = 5; 
//     }else{
//         if($menit > 120){
//         $pot = 5;  
//         }elseif($menit > 60){
//             $pot = 2.5;
//         }elseif($menit > 30){
//             $pot = 1;
//         }elseif($menit >= 1){
//             $pot = 0.5;
//         }
//     }
    
//     return $pot;
// }


// function potkecepatan($jadwal, $jamAbsen){
//     $awal  = strtotime($jadwal);
//     $akhir = strtotime($jamAbsen);
//     $diff  = $awal - $akhir;
//     $menit = floor($diff / 60);
//     $pot= '';

//     if($jamAbsen == '00:00:00'){
//         $pot = 5; 
//     }else{
//         if($menit > 120){
//         $pot = 5;  
//         }elseif($menit > 60){
//             $pot = 2.5;
//         }elseif($menit > 30){
//             $pot = 1;
//         }elseif($menit >= 1){
//             $pot = 0.5;
//         }
//     }
    
//     return $pot;
// }


// function terlambatMenit($jadwal, $jamAbsen){
//     $awal  = strtotime($jadwal);
//     $akhir = strtotime($jamAbsen);
//     $diff  = $akhir - $awal;
//     $menit = floor($diff / 60);

//     if($jamAbsen == '00:00:00'){
//         $menit = '0';
//     }else{
//         if($menit < 1){
//             $menit = '0';
//         }
//     }
    
//     return $menit;
// }


// function kecepatanMenit($jadwal, $jamAbsen){
//     $awal  = strtotime($jadwal);
//     $akhir = strtotime($jamAbsen);
//     $diff  = $awal - $akhir;
//     $menit = floor($diff / 60);

//     if($jamAbsen == '00:00:00'){
//         $menit = '0';
//     }else{
//         if($menit < 1){
//             $menit = '0';
//         }
//     }
    
    
//     return $menit;
// }


// function JamKerja($hari){
//     $this->CI =& get_instance();
//     $r = $this->CI->db->query("SELECT * FROM jam_kerja WHERE HARI='$hari'")->row_array();
//     $batasjam = $r['JAMPULANG'];
//     return $batasjam;
// }

function tidakHadir($tanggal, $kolok=""){
    $CI =& get_instance();
    if($kolok != ""){
        $queryKolok = " AND KOLOK='$kolok'";
    }else{
        $queryKolok = "";
    }
    $r = $CI->db->query("SELECT * FROM ijin_pegawai WHERE TANGGAL='$tanggal' AND (JENISIJIN = 'DL' OR JENISIJIN = 'TB' OR JENISIJIN = 'CS' OR 
    JENISIJIN = 'CB' OR JENISIJIN = 'CBsr' OR JENISIJIN = 'CP' OR JENISIJIN = 'C' OR JENISIJIN = 'DPL') $queryKolok")->num_rows();
    return $r;
}



// function getAbsenPegawai($nip='',$tanggal=''){

//     $CI =& get_instance();  
//     $a = $CI->db->query("SELECT a.userid, a.checktime, a.SN, b.badgenumber, c.kolok, c.nip, d.NAMA,'hadir' as status, e.tgl_1, 
// f.senin_m, IF(time(MIN(a.checktime))> '12:00:00','TA',time(MIN(a.checktime))) as JamMasuk, TIMEDIFF(time(MIN(a.checktime)),f.senin_m) as selisihMasuk, 

// f.senin_p, IF(time(MAX(a.checktime)) < '12:00:00' ,'TA',time(MAX(a.checktime))) as JamPulang, TIMEDIFF(f.senin_p,time(MAX(a.checktime))) as selisihPulang 

// FROM 
//             (SELECT userid, checktime, SN, '0' as jenis FROM checkinout where checktime like '2019-06-27%' UNION all SELECT userid, checktime, SN, '1' as jenis FROM checkinout_manual_2019 where checktime like '2019-06-27%') a 
//             LEFT JOIN userinfo b ON a.userid=b.userid 
//             LEFT JOIN mapping_id c ON a.SN=c.serial_number AND b.badgenumber=c.id_absen 
//             LEFT JOIN pegawai d ON c.nip=d.NIP 
//             LEFT JOIN jam_kerja_07 e ON d.NIP=e.nip 
//             LEFT JOIN jam_kerja f ON e.tgl_1=f.id_jamker
//             WHERE d.NIP='$nip' ");

//     $output = $a->result();
//     echo json_encode($output);
//     // return

// }


// function cariJamMasuk_fSpot($tanggal, $id) {
//     //cari hari 
//     $CI = &get_instance();
//     $hari = namaHari($tanggal);
//     if($hari == 'Jumat'){
//         $batas = $tanggal.' 10:00:00';
//     }else{
//         $batas = $tanggal.' 12:00:00';
//     }
//     //cari pin absen dari ID
//     $pinAbs = $CI->db->query("SELECT * FROM pegawai WHERE id_pegawai='$id'")->row_array();
//     $pin    = $pinAbs['id_absen_mesin'];
//     $kolok  = $pinAbs['kode_skpd'];
    
    
//     $absen = $CI->db->query("

//      SELECT MAX(tanggal_absensi) as masuk, 'WFH Apps' as jenis FROM absensi
//      WHERE tanggal_absensi like '$tanggal%' 
//      AND id_account='$id' 
//      AND type_absensi = 'MASUK' HAVING masuk is NOT NULL

//      union all

//      SELECT MAX(scan_date) as masuk, 'Mesin' as jenis FROM att_log
//      WHERE scan_date like '$tanggal%' 
//      AND pin='$pin' 
//      AND sn in (select sn_mesin from mesin where kolok = '$kolok' )
//      AND scan_date < '$batas' 

//      ")->row_array();

//     if(@$absen['masuk'] == ''){
//         $masuk = '-';
//     }else{
//         $jenis = "MASUK";
//         $pecah = explode(' ', $absen['masuk']);

//         if($absen['jenis'] == 'Mesin'){
//             $masuk = $pecah[1].'<br><button class="btn btn-primary btn-xs">Mesin</button>';
//         }else{
//             $masuk = $pecah[1].'<br><button class="btn btn-secondary btn-xs">WFH Apps</button>';
//         }
        

//         // '<br>
//         // <button class="btn btn-primary btn-xs" onclick="detail('.$id.', '.'\'MASUK\''.', '.'\''.$tanggal.'\''.')">detail</button>';
//     }
    
//     return $masuk;
// }


// function cariJamPulang_fSpot($tanggal, $id) {
    
//     //cari hari 
//     $CI = &get_instance();
//     $hari = namaHari($tanggal);
//     if($hari == 'Jumat'){
//         $batas = $tanggal.' 10:00:00';
//     }else{
//         $batas = $tanggal.' 12:00:00';
//     }
//     //cari pin absen dari ID
//     $pinAbs = $CI->db->query("SELECT * FROM pegawai WHERE id_pegawai='$id'")->row_array();
//     $pin    = $pinAbs['id_absen_mesin'];
//     $kolok  = $pinAbs['kode_skpd'];


//     $absen = $CI->db->query("

//      SELECT MAX(tanggal_absensi) as pulang, 'WFH Apps' as jenis FROM absensi
//      WHERE tanggal_absensi like '$tanggal%' 
//      AND id_account='$id' 
//      AND type_absensi = 'PULANG'  HAVING pulang is NOT NULL

//      union all

//      SELECT MAX(scan_date) as pulang, 'Mesin' as jenis FROM att_log
//      WHERE scan_date like '$tanggal%' 
//      AND pin='$pin' 
//      AND sn in (select sn_mesin from mesin where kolok = '$kolok' )
//      AND scan_date > '$batas' 

//      ")->row_array();
//     if(@$absen['pulang'] == ''){
//         $pulang = '-';
//     }else{
//         $pecah = explode(' ', $absen['pulang']);
//          if($absen['jenis'] == 'Mesin'){
//             $pulang = $pecah[1].'<br><button class="btn btn-primary btn-xs">Mesin</button>';
//         }else{
//             $pulang = $pecah[1].'<br><button class="btn btn-secondary btn-xs">WFH Apps</button>';
//         }

//         // $pulang = $pecah[1].'<br>
//         // <button class="btn btn-primary btn-xs" onclick="detail('.$id.', '.'\'PULANG\''.', '.'\''.$tanggal.'\''.')">detail</button>';
//     }
    
//     return $pulang;
// }




// function cariJamMasuk_soltn($tanggal, $id) {
//     //cari hari 
//     $hari = namaHari($tanggal);
//     if($hari == 'Jumat'){
//         $batas = $tanggal.' 10:00:00';
//     }else{
//         $batas = $tanggal.' 12:00:00';
//     }
    
//     $CI = &get_instance();
//     $absen = $CI->db->query("

//      SELECT MAX(tanggal_absensi) as masuk, 'WFH Apps' as jenis FROM absensi
//      WHERE tanggal_absensi like '$tanggal%' 
//      AND id_account='$id' 
//      AND type_absensi = 'MASUK' HAVING masuk is NOT NULL

//      union all

//      SELECT MAX(tanggal_absensi) as masuk, 'Mesin' as jenis FROM absensi_mesin
//      WHERE tanggal_absensi like '$tanggal%' 
//      AND id_account='$id' 
//      AND type_absensi = 'MASUK' HAVING masuk is NOT NULL

//      ")->row_array();

//     if(@$absen['masuk'] == ''){
//         $masuk = '-';
//     }else{
//         $jenis = "MASUK";
//         $pecah = explode(' ', $absen['masuk']);
//          if($absen['jenis'] == 'Mesin'){
//             $masuk = $pecah[1].'<br><button class="btn btn-primary btn-xs">Mesin</button>';
//         }else{
//             $masuk = $pecah[1].'<br><button class="btn btn-secondary btn-xs">WFH Apps</button>';
//         }
//     }
    
//     return $masuk;
// }


// function cariJamPulang_soltn($tanggal, $id) {
//     $batas = $tanggal.' 12:00:00';
//     $CI = &get_instance();
//     $absen = $CI->db->query("

//      SELECT MAX(tanggal_absensi) as pulang, 'WFH Apps' as jenis FROM absensi
//      WHERE tanggal_absensi like '$tanggal%' 
//      AND id_account='$id' 
//      AND type_absensi = 'PULANG'  HAVING pulang is NOT NULL

//      union all

//      SELECT MAX(tanggal_absensi) as pulang, 'Mesin' as jenis FROM absensi_mesin
//      WHERE tanggal_absensi like '$tanggal%' 
//      AND id_account='$id' 
//      AND type_absensi = 'PULANG'  HAVING pulang is NOT NULL

//      ")->row_array();
//     if(@$absen['pulang'] == ''){
//         $pulang = '-';
//     }else{
//         $pecah = explode(' ', $absen['pulang']);

//          if($absen['jenis'] == 'Mesin'){
//             $pulang = $pecah[1].'<br><button class="btn btn-primary btn-xs">Mesin</button>';
//         }else{
//             $pulang = $pecah[1].'<br><button class="btn btn-secondary btn-xs">WFH Apps</button>';
//         }
//     }
    
//     return $pulang;
// }
